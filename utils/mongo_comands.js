// - кол-во твитов 100
db.timeline.find({timeline: {$size: 100}});

//- вернуть только id
db.timeline.findOne({timeline: {$size: 0}}, {userId: 1});

//- текст твитов
db.timeline.findOne({timeline: {$size: 2}}, {userId: 1, timeline: 1, "timeline.text": 1});

//- твиты с сообщениями
db.timeline.findOne({timeline: {$size: 2}}, {
    userId: 1,
    timeline: 1,
    "timeline.text": 1,
    "timeline.id": 1,
    "timeline.retweeted_status.text": 1,
    "timeline.retweeted_status.user": 1,
    "timeline.retweeted_status.id": 1
});

//Новый запрос:
db.timeline.findOne({timeline: {$exists: true}, $where: 'this.timeline.length>1'}, {
    userId: 1,
    timeline: 1,
    "timeline.text": 1,
    "timeline.id": 1,
    "timeline.retweeted_status.text": 1,
    "timeline.retweeted_status.user": 1,
    "timeline.retweeted_status.id": 1,
    "timeline.lang": 1
});

db.timeline.findOne({timeline: {$exists: true}, "timeline.lang": "es", $where: 'this.timeline.length>1'}, {
    userId: 1,
    timeline: 1,
    "timeline.text": 1,
    "timeline.id": 1,
    "timeline.retweeted_status.text": 1,
    "timeline.retweeted_status.user": 1,
    "timeline.retweeted_status.id": 1,
    "timeline.lang": 1
});

//- найти фолловеров
db.followers.find({userId: {$in: [NumberLong("2185662621")]}});


db.timeline.findOne({
        timeline: {$exists: true}, $where: function () {
            return this.timeline.filter(function f(el) {
                    return (el.lang == "und");
                }).length > 100
        }, "timeline.lang": "und"
    },
    {
        userId: 1,
        timeline: 1,
        "timeline.text": 1,
        "timeline.id": 1,
        "timeline.retweeted_status.text": 1,
        "timeline.retweeted_status.user": 1,
        "timeline.retweeted_status.id": 1,
        "timeline.lang": 1,
        timeline: {$elemMatch: {lang: "und"}}
    });

//Работает, но возвращает много (хотелось бы твиты, в которых только нужный lang)
db.timeline.findOne({
        timeline: {$exists: true}, $where: function () {
            return this.timeline.filter(function (elem) {
                    return (elem.lang == "en");
                }).length >= 10;
        }
    },
    {
        userId: 1,
        timeline: 1,
        "timeline.text": 1,
        "timeline.id": 1,
        "timeline.retweeted_status.text": 1,
        "timeline.retweeted_status.user": 1,
        "timeline.retweeted_status.id": 1,
        "timeline.lang": 1
    });


//найти юзеров для тестирования сопоставления
db.profile.find({
    "profile.lang": "en",
    "profile.description": / like /,
    "profile.statuses_count": {$gt: 100}
}, {profile: 1, userId: 1, "profile.description": 1, "profile.screen_name": 1}).limit(10).pretty()


//test
db.timeline.findOne({
        timeline: {$exists: true}, $where: function () {
            return this.timeline.filter(function (elem) {
                    return (elem.lang == "und");
                }).length == 3;
        },
        timeline: {$elemMatch: {lang: "und"}}
    },
    {
        "timeline.$": 1
    });

db.timeline.findOne({
        timeline: {$exists: true},
        $where: "function () {return this.timeline.filter(function (elem) {return (elem.lang == \"und\");}).length == 3;}"
    },
    {
        userId: 1,
        timeline: 1,
        "timeline.text": 1,
        "timeline.id": 1,
        "timeline.retweeted_status.text": 1,
        "timeline.retweeted_status.user": 1,
        "timeline.retweeted_status.id": 1,
        "timeline.lang": 1
    });

db.timeline.aggregate(
    {
        $match: {
            $and: [
                {timeline: {$exists: true}}
            ]
        }
    }
)
;


//find who retweet tweet of this user
db.timeline.findOne({"timeline.retweeted_status.id": 248338551152996353})