package alex.interests.utils;

import alex.interests.data.model.Topic;
import alex.interests.process.mapping.entities.Category;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.type.MapLikeType;

import java.io.File;
import java.io.IOException;
import java.util.Comparator;
import java.util.SortedMap;
import java.util.TreeMap;

public class JsonSerializer {

    private static final ObjectMapper objectMapper = new ObjectMapper()
            .configure(SerializationFeature.INDENT_OUTPUT, true)
            .configure(SerializationFeature.WRITE_NULL_MAP_VALUES, true)
            .setSerializationInclusion(JsonInclude.Include.NON_NULL);

    private static final MapLikeType sortedMapTopicType = objectMapper
            .getTypeFactory()
            .constructMapLikeType(SortedMap.class, Double.class, Topic.class);

    private static final MapLikeType sortedMapCategoryType = objectMapper
            .getTypeFactory()
            .constructMapLikeType(SortedMap.class, Double.class, Category.class);

    public static SortedMap<Double, Topic> readTopics(String fileName) {
        File file = new File(fileName);
        if (file.exists()) {
            try {
                SortedMap<Double, Topic> calculatedTopicsRead = objectMapper.readValue(file, sortedMapTopicType);
                SortedMap<Double, Topic> calculatedTopics = new TreeMap<>(Comparator.reverseOrder());
                calculatedTopics.putAll(calculatedTopicsRead);
                return calculatedTopics;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return null;
    }

    public static void write(String fileName, Object value) {
        File file = new File(fileName);
        try {
            file.createNewFile();
            objectMapper.writeValue(file, value);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }



    public static SortedMap<Double, Category> readCategories(String fileName) {
        File file = new File(fileName);
        if (file.exists()) {
            try {
                SortedMap<Double, Category> categoriesRead = objectMapper.readValue(file, sortedMapCategoryType);
                SortedMap<Double, Category> categories = new TreeMap<>(Comparator.reverseOrder());
                categories.putAll(categoriesRead);
                return categories;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return null;
    }


}
