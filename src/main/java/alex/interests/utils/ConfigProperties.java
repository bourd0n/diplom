package alex.interests.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigProperties {

    private static final Properties CONFIG = new Properties();
    private static final ConfigProperties INSTANCE = new ConfigProperties();

    private ConfigProperties() {
        try (InputStream propsResource = getClass().getResourceAsStream("/config.properties")) {
            CONFIG.load(propsResource);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String getBasicPath() {
        return getProperty("file.basicPath");
    }

    public static ConfigProperties getInstance() {
        return INSTANCE;
    }

    public String getProperty(String key) {
        return CONFIG.getProperty(key);
    }
}
