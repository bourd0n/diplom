package alex.interests.data.extractor;

import alex.interests.utils.ConfigProperties;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import java.net.UnknownHostException;

public class LocalMongoExtractor extends MongoExtractor implements DataExtractor {

    private static volatile LocalMongoExtractor INSTANCE;

    @Override
    protected void init() {
        try {
            ConfigProperties CONFIG = ConfigProperties.getInstance();
            String cacheEnabled = CONFIG.getProperty("cache.enabled");
            isCacheEnabled = Boolean.valueOf(cacheEnabled);
            tweetsCount = Integer.parseInt(CONFIG.getProperty("tweets.count"));
            tweetsLang = CONFIG.getProperty("tweets.lang");
            client = new MongoClient();
            db = client.getDB(getDbName());
            checkUserExistence = false;
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    }

    public static MongoExtractor getInstance() {
        LocalMongoExtractor localInstance = INSTANCE;
        if (localInstance == null) {
            synchronized (MongoExtractor.class) {
                localInstance = INSTANCE;
                if (localInstance == null) {
                    INSTANCE = localInstance = new LocalMongoExtractor();
                    INSTANCE.init();
                }
            }
        }
        return localInstance;
    }

    @Override
    protected DBObject getTimelineFields() {
        return MongoQueries.TWEETS_FIELDS_LOCAL;
    }

    @Override
    protected String getUserIdKey() {
        return "id";
    }

    @Override
    protected String getDbName() {
        return "im-twitter";
    }

    @Override
    protected Object getValueFromDB(DBObject dbObject, String key) {
        return ((DBObject) dbObject.get("value")).get("object");
    }
}
