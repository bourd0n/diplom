package alex.interests.data.extractor;

import alex.interests.data.model.Profile;
import alex.interests.data.model.ReTweet;
import alex.interests.data.model.Tweet;
import alex.interests.data.model.User;
import com.google.common.collect.ListMultimap;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface DataExtractor {

    Profile loadUserProfile(final String userId);

    Collection<User> getUserWithTweets(int count);

    Collection<User> getUserWithTweetsAndReferences(int count);

    Collection<String> loadUserFriends(String userId);

    Collection<String> loadUserFollowers(String userId);

    //Map<String, Integer> loadUserFollowersCountBulk(Collection<String> userIds);

    Collection<User> getUsersByIds(Collection<String> ids);

    User getUserById(String id);

    User getUserByIdWithoutReferences(String id);

    List<Tweet> loadUserTweets(String id);

    ListMultimap<String, ReTweet> loadRetweetedUsers(String userId);

}