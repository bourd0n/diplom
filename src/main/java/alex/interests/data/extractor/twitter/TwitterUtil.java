package alex.interests.data.extractor.twitter;


import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.pool2.ObjectPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.*;

import java.util.*;

public class TwitterUtil {
    private static final Logger log = LoggerFactory.getLogger(TwitterUtil.class);

    private ObjectPool<Twitter> pool;

    public TwitterUtil(ObjectPool<Twitter> pool) {
        this.pool = pool;
    }

    public List<Status> getTimeline(String userName) {
        List<Status> statuses = new ArrayList<>();
        int pageNum = 1;
        Twitter twitter = null;
        try {
            twitter = pool.borrowObject();
            while (true) {
                try {

                    int size = statuses.size();
                    Paging page = new Paging(pageNum++, 200);
                    statuses.addAll(twitter.getUserTimeline(userName, page));
                    if (statuses.size() == size)
                        break;
                } catch (TwitterException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            throw new RuntimeException("Exception occurred during getTimeline", e);
        } finally {
            if (twitter != null) {
                try {
                    pool.returnObject(twitter);
                } catch (Exception e) {
                    log.error("Exception during return object to pool", e);
                }
            }
        }
        return statuses;
    }

    private Set<Long> getRetweetedUsers(long statusId) throws TwitterException {
        Set<Long> idsSet = new HashSet<>();
        long cursor = -1;
        IDs ids;
        Twitter twitter = null;
        log.debug("Listing following ids.");
        try {
            twitter = pool.borrowObject();
            do {
                ids = twitter.getRetweeterIds(statusId, 500, cursor);
                idsSet.addAll(Arrays.asList(ArrayUtils.toObject(ids.getIDs())));
            } while ((cursor = ids.getNextCursor()) != 0);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (twitter != null) {
                try {
                    pool.returnObject(twitter);
                } catch (Exception e) {
                    log.error("Exception during return object to pool", e);
                }
            }
        }
        return idsSet;
    }
}
