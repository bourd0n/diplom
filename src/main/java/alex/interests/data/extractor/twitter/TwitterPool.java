package alex.interests.data.extractor.twitter;

import org.apache.commons.pool2.ObjectPool;
import org.apache.commons.pool2.PooledObjectFactory;
import org.apache.commons.pool2.impl.AbandonedConfig;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import twitter4j.Twitter;

public class TwitterPool extends GenericObjectPool<Twitter> implements ObjectPool<Twitter> {
    public TwitterPool(PooledObjectFactory<Twitter> factory) {
        super(factory);
    }

    public TwitterPool(PooledObjectFactory<Twitter> factory, GenericObjectPoolConfig config) {
        super(factory, config);
    }

    public TwitterPool(PooledObjectFactory<Twitter> factory, GenericObjectPoolConfig config, AbandonedConfig abandonedConfig) {
        super(factory, config, abandonedConfig);
    }


}
