package alex.interests.data.extractor.twitter;

import alex.interests.data.extractor.DataExtractor;
import alex.interests.data.model.Profile;
import alex.interests.data.model.ReTweet;
import alex.interests.data.model.Tweet;
import alex.interests.data.model.User;
import com.google.common.collect.ListMultimap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;

import java.util.Collection;
import java.util.List;

public class TwitterExtractor implements DataExtractor {
    private static final Logger log = LoggerFactory.getLogger(TwitterExtractor.class);

    private static final TwitterExtractor INSTANCE = new TwitterExtractor();

    private static int tweetsCount;
    private static String tweetsLang;
    private static final Twitter twitter = TwitterFactory.getSingleton();

    private TwitterExtractor() {
    }

    public static TwitterExtractor getInstance() {
        return INSTANCE;
    }

    @Override
    public Profile loadUserProfile(String userId) {
        return null;
    }

    @Override
    public Collection<User> getUserWithTweets(int count) {
        return null;
    }

    @Override
    public Collection<User> getUserWithTweetsAndReferences(int count) {
        return null;
    }

    @Override
    public Collection<String> loadUserFriends(String userId) {
        return null;
    }

    @Override
    public Collection<String> loadUserFollowers(String userId) {
        return null;
    }

    @Override
    public Collection<User> getUsersByIds(Collection<String> ids) {
        return null;
    }

    @Override
    public User getUserById(String id) {
        return null;
    }

    @Override
    public User getUserByIdWithoutReferences(String id) {
        return null;
    }

    @Override
    public List<Tweet> loadUserTweets(String id) {
        return null;
    }

    @Override
    public ListMultimap<String, ReTweet> loadRetweetedUsers(String userId) {
        return null;
    }
}