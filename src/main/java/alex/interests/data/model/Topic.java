package alex.interests.data.model;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.*;

public class Topic implements Serializable {

    private String id;
    private SortedMap<Double, String> termsDistribution = new TreeMap<>(Comparator.reverseOrder());
    //private Interest interest;

    //only for serializing
    @Deprecated
    protected Topic() {
    }

    public Topic(String id, SortedMap<Double, String> termsDistribution) {
        this.id = id;
        this.termsDistribution.putAll(termsDistribution);
    }

    public Topic(SortedMap<Double, String> termsDistribution) {
        this.id = UUID.randomUUID().toString();
        this.termsDistribution.putAll(termsDistribution);
    }

    public String getId() {
        return id;
    }

    public SortedMap<Double, String> getTermsDistribution() {
        return Collections.unmodifiableSortedMap(termsDistribution);
    }

    //public Interest getInterest() {
    //    return interest;
    //}
    //
    //public boolean isInterestCalculated() {
    //    return interest != null;
    //}

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("Topic {");
        DecimalFormat df = new DecimalFormat("#.#");
        for (Map.Entry<Double, String> termDistributionEntry : termsDistribution.entrySet()) {
            stringBuilder.append(termDistributionEntry.getValue())
                    .append("(")
                    .append(df.format(termDistributionEntry.getKey()))
                    .append(")")
                    .append(" ");
        }
        stringBuilder.append("}");
        return stringBuilder.toString();
    }
}