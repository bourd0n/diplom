package alex.interests.data.model;

import java.util.*;

public class Interest {

	private final String id;
	private final String name;
	private final Collection<Interest> subInterests = new ArrayList<>();

    public Interest(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}