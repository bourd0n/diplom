package alex.interests.data.model;

public enum UserField {
    ID, NAME, PROFILE, FRIENDS, FOLLOWERS, RETWEETED_USERS, TWEETS
}
