package alex.interests.data.model;

import com.google.common.base.Preconditions;

import java.util.Collection;

public class ReTweet extends Tweet {
    private final String retweetedUserId;
    private final String retweetedTweetId;

    public ReTweet(Long id, String text, Long userId, Collection<String> hashtags, Long retweetedUserId, Long retweetedTweetId) {
        super(id, text, userId, hashtags);
        Preconditions.checkNotNull(retweetedUserId);
        Preconditions.checkNotNull(retweetedTweetId);
        this.retweetedUserId = retweetedUserId.toString();
        this.retweetedTweetId = retweetedTweetId.toString();
    }

    public ReTweet(String id, String text, String userId, Collection<String> hashtags, String retweetedUserId, String retweetedTweetId) {
        super(id, text, userId, hashtags);
        Preconditions.checkNotNull(retweetedUserId);
        this.retweetedUserId = retweetedUserId;
        this.retweetedTweetId = retweetedTweetId;
    }

    public String getRetweetedUserId() {
        return retweetedUserId;
    }

    public String getRetweetedTweetId() {
        return retweetedTweetId;
    }

    @Override
    public String toString() {
        return "ReTweet{" +
                "id='" + id + '\'' +
                ", text='" + text + '\'' +
                ", userId='" + userId + '\'' +
                ", hashtags=" + hashtags +
                "retweetedUserId='" + retweetedUserId + '\'' +
                ", retweetedTweetId='" + retweetedTweetId + '\'' +
                '}';
    }
}
