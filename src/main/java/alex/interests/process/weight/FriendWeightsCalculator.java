package alex.interests.process.weight;

import alex.interests.data.extractor.DataExtractor;
import alex.interests.data.model.User;
import alex.interests.process.params.AlgorithmParams;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Sets;
import com.google.common.collect.Table;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class FriendWeightsCalculator implements LinkWeightCalculator {
    private static final Logger log = LoggerFactory.getLogger(FriendWeightsCalculator.class);

    private final DataExtractor dataExtractor;
    private final AlgorithmParams algorithmParams;

    public FriendWeightsCalculator(DataExtractor dataExtractor, AlgorithmParams algorithmParams) {
        this.algorithmParams = algorithmParams;
        this.dataExtractor = dataExtractor;
    }

    //current implementation assume that weight can be 1.0 or 0.0 (no link)
    public Map<String, Double> calculateOutLinkWeights(User sourceUser) {
        Set<String> sourceUserFriends = sourceUser.getFriends(true);
        String sourceUserId = sourceUser.getId();
        log.debug("Start calculate authority weights for user {}", sourceUser);
        //calculate Wr({u,v}, a) table
        Table<String, String, Double> edgeWeightsTable = HashBasedTable.create();
        //wr(u,u)=1
        edgeWeightsTable.put(sourceUserId, sourceUserId, 1.0);
        for (String friendId : sourceUserFriends) {
            edgeWeightsTable.put(sourceUserId, friendId, edgeWeightFromSourceUserToItsFriend(sourceUserId, friendId));
            //wr(v,v)=1
            edgeWeightsTable.put(friendId, friendId, 1.0);
            //final User friendById = dataExtractor.getUserById(friendId);
            //if (friendById == null)
                //in mongo some users not exists
                //continue;
            //Set<String> friendsOfSourceFriend = friendById.getFriends(true);
            Collection<String> friendsOfSourceFriend = dataExtractor.loadUserFriends(friendId);
            friendsOfSourceFriend.stream()
                    .filter(sourceUserFriends::contains)
                    .forEach(friendOfSourceFriend ->
                            edgeWeightsTable.put(friendId, friendOfSourceFriend, edgeWeighFromFollowerToItsFriend(friendId, sourceUserId)));
        }
        log.debug("authority edgeWeightsTable constructed. Size {}", edgeWeightsTable.size());
        Map<String, Double> friendId2Weight = new HashMap<>();
        //calculate Wa(u,v)
        Double weightRSquareSourceUserSum = sumOfSquareWeighRForUser_authority(sourceUserId, edgeWeightsTable);
        Map<String, Double> authorityForSourceUserToWeight = edgeWeightsTable.row(sourceUserId);
        for (String friendId : sourceUserFriends) {
            Double weightRSquareFollowerSum = getSumOfSquareWeighRForFriendUser_authority(friendId);
            if (weightRSquareFollowerSum == 0) {
                //in mongo some users not exists
                continue;
            }
            Double normalizationCoef = Math.sqrt(weightRSquareFollowerSum * weightRSquareSourceUserSum);
            Map<String, Double> authorityForFriendToWeight = edgeWeightsTable.row(friendId);
            Sets.SetView<String> keyIntersection = Sets.intersection(authorityForSourceUserToWeight.keySet(), authorityForFriendToWeight.keySet());
            Double sumOfWeightMult = keyIntersection.stream()
                    .mapToDouble(commonAuthId -> authorityForSourceUserToWeight.get(commonAuthId) * authorityForFriendToWeight.get(commonAuthId))
                    .sum();
            friendId2Weight.put(friendId, sumOfWeightMult / normalizationCoef);
        }
        log.debug("authority friendId2Weight calculated. Size {}", friendId2Weight.size());
        return friendId2Weight;
    }

    //current implementation assume that weight can be 1.0 or 0.0 (no link)
    public Map<String, Double> calculateInLinkWeights(User sourceUser) {
        final Set<String> sourceUserFriends = sourceUser.getFriends(true);
        final Set<String> sourceUserFollowers = sourceUser.getFollowers(true);
        String sourceUserId = sourceUser.getId();
        log.debug("Start calculate hub weights for user {}", sourceUser);
        //calculate Wr(h, {u,v}) table
        Table<String, String, Double> edgeWeightsTable = HashBasedTable.create();
        //wr(u,u)=1
        edgeWeightsTable.put(sourceUserId, sourceUserId, 1.0);
        for (String sourceUserFollowerId : sourceUserFollowers) {
            edgeWeightsTable.put(sourceUserFollowerId, sourceUserId, 1.0);
        }
        for (String friendId : sourceUserFriends) {
            //wr(v,v)=1
            edgeWeightsTable.put(friendId, friendId, 1.0);
            edgeWeightsTable.put(sourceUserId, friendId, 1.0);
            //final User friendById = dataExtractor.getUserById(friendId);
            //if (friendById == null)
                //in mongo some users not exists
                //continue;
            //Set<String> friendFollowers = friendById.getFollowers(true);
            Collection<String> friendFollowers = dataExtractor.loadUserFollowers(friendId);
            friendFollowers.stream()
                    .filter(sourceUserFollowers::contains)
                    .forEach(friendFollower ->
                            edgeWeightsTable.put(friendFollower, friendId, 1.0));
        }
        log.debug("hubs edgeWeightsTable constructed. Size {}", edgeWeightsTable.size());
        Map<String, Double> friendId2Weight = new HashMap<>();
        //calculate Wa(u,v)
        Double weightRSquareSourceUserSum = sumOfSquareWeighRForUser_hubs(sourceUserId, edgeWeightsTable);
        Map<String, Double> hubsForSourceUserToWeight = edgeWeightsTable.column(sourceUserId);
        for (String friendId : sourceUserFriends) {
            Double weightRSquareFollowerSum = getSumOfSquareWeighRForFriendUser_hubs(friendId);
            if (weightRSquareFollowerSum == 0) {
                //in mongo some users not exists
                continue;
            }
            Double normalizationCoef = Math.sqrt(weightRSquareFollowerSum * weightRSquareSourceUserSum);
            Map<String, Double> hubForFriendToWeight = edgeWeightsTable.column(friendId);
            Sets.SetView<String> keyIntersection = Sets.intersection(hubsForSourceUserToWeight.keySet(), hubForFriendToWeight.keySet());
            Double sumOfWeightMult = keyIntersection.stream()
                    .mapToDouble(commonHubId -> hubsForSourceUserToWeight.get(commonHubId) * hubForFriendToWeight.get(commonHubId))
                    .sum();
            friendId2Weight.put(friendId, sumOfWeightMult / normalizationCoef);
        }
        log.debug("hubs friendId2Weight calculated. Size {}", friendId2Weight.size());
        return friendId2Weight;
    }

    private Double getSumOfSquareWeighRForFriendUser_authority(String friendId) {
        //final User friendById = dataExtractor.getUserById(friendId);
        //if (friendById == null) {
        //    return 0.0;
        //}
        //final Set<String> friendsOfFriend = friendById.getFriends(true);
        final Collection<String> friendsOfFriend = dataExtractor.loadUserFriends(friendId);
        //because value is 1.0 we could do so
        return (double) friendsOfFriend.size() + 1 /*request friend*/;
    }

    private Double getSumOfSquareWeighRForFriendUser_hubs(String friendId) {
        //final User friendById = dataExtractor.getUserById(friendId);
        //if (friendById == null) {
        //    return 0.0;
        //}
        //final Set<String> friendFollowers = friendById.getFollowers(true);
        final Collection<String> friendFollowers = dataExtractor.loadUserFollowers(friendId);
        //because value is 1.0 we could do so
        return (double) friendFollowers.size() + 1 /*request friend*/;
    }

    private Double edgeWeighFromFollowerToItsFriend(String followerId, String followerFriendId) {
        return 1.0;
    }

    private Double edgeWeightFromSourceUserToItsFriend(String sourceUserId, String sourceUserFriend) {
        return 1.0;
    }

    private Double sumOfSquareWeighRForUser_authority(String userId, Table<String, String, Double> edgeWeightsTable) {
        return edgeWeightsTable
                .row(userId)
                .values()
                .stream()
                .mapToDouble(weight -> weight * weight)
                .sum();
    }

    private Double sumOfSquareWeighRForUser_hubs(String userId, Table<String, String, Double> edgeWeightsTable) {
        return edgeWeightsTable
                .column(userId)
                .values()
                .stream()
                .mapToDouble(weight -> weight * weight)
                .sum();
    }
}
