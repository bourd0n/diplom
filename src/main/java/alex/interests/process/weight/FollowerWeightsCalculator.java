package alex.interests.process.weight;

import alex.interests.data.extractor.DataExtractor;
import alex.interests.data.model.User;
import alex.interests.process.params.AlgorithmParams;
import com.google.common.collect.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class FollowerWeightsCalculator implements LinkWeightCalculator {
    private static final Logger log = LoggerFactory.getLogger(FollowerWeightsCalculator.class);

    private final DataExtractor dataExtractor;
    private final AlgorithmParams algorithmParams;

    public FollowerWeightsCalculator(DataExtractor dataExtractor, AlgorithmParams algorithmParams) {
        this.algorithmParams = algorithmParams;
        this.dataExtractor = dataExtractor;
    }

    //current implementation assume that weight can be 1.0 or 0.0 (no link)
    public Map<String, Double> calculateOutLinkWeights(User sourceUser) {
        Set<String> followers = sourceUser.getFollowers(true);
        Set<String> sourceUserFriends = sourceUser.getFriends(true);
        String sourceUserId = sourceUser.getId();
        log.debug("Start calculate authority weights for user {}", sourceUser);
        //calculate Wr({u,v}, a) table
        Table<String, String, Double> edgeWeightsTable = HashBasedTable.create();
        //wr(u,u)=1
        edgeWeightsTable.put(sourceUserId, sourceUserId, 1.0);
        for (String sourceUserFriend : sourceUserFriends) {
            edgeWeightsTable.put(sourceUserId, sourceUserFriend, edgeWeightFromSourceUserToItsFriend(sourceUserId, sourceUserFriend));
        }
        for (String followerId : followers) {
            //wr(v,v)=1
            edgeWeightsTable.put(followerId, followerId, 1.0);
            edgeWeightsTable.put(followerId, sourceUserId, edgeWeighFromFollowerToItsFriend(followerId, sourceUserId));
            //final User followerById = dataExtractor.getUserById(followerId);
            //if (followerById == null)
            //    in mongo some users not exists
                //continue;
            Collection<String> followerFriends = dataExtractor.loadUserFriends(followerId);
            followerFriends.stream()
                    .filter(sourceUserFriends::contains)
                    .forEach(followerFriend ->
                            edgeWeightsTable.put(followerId, followerFriend, edgeWeighFromFollowerToItsFriend(followerId, sourceUserId)));
        }
        log.debug("authority edgeWeightsTable constructed. Size {}", edgeWeightsTable.size());
        Map<String, Double> followerId2Weight = new HashMap<>();
        //calculate Wa(u,v)
        Double weightRSquareSourceUserSum = sumOfSquareWeighRForUser_authority(sourceUserId, edgeWeightsTable);
        Map<String, Double> authorityForSourceUserToWeight = edgeWeightsTable.row(sourceUserId);
        for (String followerId : followers) {
            Double weightRSquareFollowerSum = getSumOfSquareWeighRForFollowerUser_authority(followerId);
            if (weightRSquareFollowerSum == 0) {
                //in mongo some users not exists
                continue;
            }
            Double normalizationCoef = Math.sqrt(weightRSquareFollowerSum * weightRSquareSourceUserSum);
            Map<String, Double> authorityForFollowerToWeight = edgeWeightsTable.row(followerId);
            Sets.SetView<String> keyIntersection = Sets.intersection(authorityForSourceUserToWeight.keySet(), authorityForFollowerToWeight.keySet());
            Double sumOfWeightMult = keyIntersection.stream()
                    .mapToDouble(commonAuthId -> authorityForSourceUserToWeight.get(commonAuthId) * authorityForFollowerToWeight.get(commonAuthId))
                    .sum();
            followerId2Weight.put(followerId, sumOfWeightMult / normalizationCoef);
        }
        log.debug("authority followerId2Weight calculated. Size {}", followerId2Weight.size());
        return followerId2Weight;
    }

    //current implementation assume that weight can be 1.0 or 0.0 (no link)
    public Map<String, Double> calculateInLinkWeights(User sourceUser) {
        Set<String> sourceUserFollowers = sourceUser.getFollowers(true);
        String sourceUserId = sourceUser.getId();
        log.debug("Start calculate hub weights for user {}", sourceUser);
        //calculate Wr(h, {u,v}) table
        Table<String, String, Double> edgeWeightsTable = HashBasedTable.create();
        //wr(u,u)=1
        edgeWeightsTable.put(sourceUserId, sourceUserId, 1.0);
        for (String sourceFollowerId : sourceUserFollowers) {
            edgeWeightsTable.put(sourceFollowerId, sourceUserId, 1.0);
            //wr(v,v)=1
            edgeWeightsTable.put(sourceFollowerId, sourceFollowerId, 1.0);
            //final User sourceFollowerById = dataExtractor.getUserById(sourceFollowerId);
            //if (sourceFollowerById == null)
                //some users not exists in db
                //continue;
            Collection<String> followersOfSourceFollower = dataExtractor.loadUserFollowers(sourceFollowerId);
            followersOfSourceFollower.stream()
                    .filter(sourceUserFollowers::contains)
                    .forEach(followerOfSourceFollower ->
                            edgeWeightsTable.put(followerOfSourceFollower, sourceFollowerId, 1.0));
        }
        log.debug("hubs edgeWeightsTable constructed. Size {}", edgeWeightsTable.size());
        Map<String, Double> followerId2Weight = new HashMap<>();
        //calculate Wa(u,v)
        Double weightRSquareSourceUserSum = sumOfSquareWeighRForUser_hubs(sourceUserId, edgeWeightsTable);
        Map<String, Double> hubsForSourceUserToWeight = edgeWeightsTable.column(sourceUserId);
        for (String followerId : sourceUserFollowers) {
            Double weightRSquareFollowerSum = getSumOfSquareWeighRForFollowerUser_hubs(followerId);
            if (weightRSquareFollowerSum == 0) {
                //in mongo some users not exists
                continue;
            }
            Double normalizationCoef = Math.sqrt(weightRSquareFollowerSum * weightRSquareSourceUserSum);
            Map<String, Double> hubForFollowerToWeight = edgeWeightsTable.column(followerId);
            Sets.SetView<String> keyIntersection = Sets.intersection(hubsForSourceUserToWeight.keySet(), hubForFollowerToWeight.keySet());
            Double sumOfWeightMult = keyIntersection.stream()
                    .mapToDouble(commonHubId -> hubsForSourceUserToWeight.get(commonHubId) * hubForFollowerToWeight.get(commonHubId))
                    .sum();
            followerId2Weight.put(followerId, sumOfWeightMult / normalizationCoef);
        }
        log.debug("hubs followerId2Weight calculated. Size {}", followerId2Weight.size());
        return followerId2Weight;
    }

    private Double getSumOfSquareWeighRForFollowerUser_authority(String followerId) {
        //final User followerById = dataExtractor.getUserById(followerId);
        //if (followerById == null) {
        //    return 0.0;
        //}
        //final Set<String> followerFriends = followerById.getFriends(true);
        final Collection<String> followerFriends = dataExtractor.loadUserFriends(followerId);
        //because value is 1.0 we could do so
        return (double) followerFriends.size() + 1 /*requested follower*/;
    }

    private Double getSumOfSquareWeighRForFollowerUser_hubs(String followerId) {
        //final User followerById = dataExtractor.getUserById(followerId);
        //if (followerById == null) {
        //    return 0.0;
        //}
        //final Set<String> followersOfFollower = followerById.getFollowers(true);
        final Collection<String> followersOfFollower = dataExtractor.loadUserFollowers(followerId);
        //because value is 1.0 we could do so
        return (double) followersOfFollower.size() + 1 /*requested follower*/;
    }

    private Double edgeWeighFromFollowerToItsFriend(String followerId, String followerFriendId) {
        return 1.0;
    }

    private Double edgeWeightFromSourceUserToItsFriend(String sourceUserId, String sourceUserFriend) {
        return 1.0;
    }

    private Double sumOfSquareWeighRForUser_authority(String userId, Table<String, String, Double> edgeWeightsTable) {
        return edgeWeightsTable
                .row(userId)
                .values()
                .stream()
                .mapToDouble(weight -> weight * weight)
                .sum();
    }

    private Double sumOfSquareWeighRForUser_hubs(String userId, Table<String, String, Double> edgeWeightsTable) {
        return edgeWeightsTable
                .column(userId)
                .values()
                .stream()
                .mapToDouble(weight -> weight * weight)
                .sum();
    }
}
