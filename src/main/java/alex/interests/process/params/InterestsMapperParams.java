package alex.interests.process.params;

public class InterestsMapperParams {
    private final int numTopTopics;
    private final double thresholdSenses;
    private final int parentCategoriesRecursiveLevel;

    public static final InterestsMapperParams DEFAULT_MAPPER_PARAMS = new InterestsMapperParams(15, 0.3, 2);

    public InterestsMapperParams(int numTopTopics, double thresholdSenses, int parentCategoriesRecursiveLevel) {
        this.numTopTopics = numTopTopics;
        this.thresholdSenses = thresholdSenses;
        this.parentCategoriesRecursiveLevel = parentCategoriesRecursiveLevel;
    }

    public int getNumTopTopics() {
        return numTopTopics;
    }

    public double getThresholdSenses() {
        return thresholdSenses;
    }

    public int getParentCategoriesRecursiveLevel() {
        return parentCategoriesRecursiveLevel;
    }
}
