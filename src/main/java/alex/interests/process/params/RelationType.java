package alex.interests.process.params;

public enum RelationType {
    FOLLOWING, RETWEETS, NONE
}