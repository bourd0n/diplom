package alex.interests.process.params;

public class AlgorithmParams {

    private final int numThreads;
    private final int numIterations;
    private final int numTopics;
    private final double alpha;
    private final double beta;
    private final double threshold;
    private final double coefOutLinks;
    private final double coefInLinks;
    private final double coefOwn;
    private final double trainTweetsRatio;
    private final TopicInferenceParams topicInferenceParams;
    private final InterestsMapperParams interestsMapperParams;

    public static final AlgorithmParams DEFAULT_ALGORITHM_PARAMS = new AlgorithmParams();

    private AlgorithmParams(){
        numThreads = 4;
        numIterations = 1300;
        numTopics = 50;
        alpha = 0.3;
        beta = 0.01;
        threshold = 0.001;
        coefOutLinks = 0.05;
        coefInLinks = 0.05;
        coefOwn = 0.9;
        trainTweetsRatio = 0.9;
        interestsMapperParams = InterestsMapperParams.DEFAULT_MAPPER_PARAMS;
        topicInferenceParams = TopicInferenceParams.DEFAULT_INFERENCE_PARAMS;
    }

    private AlgorithmParams(AlgorithmParamsBuilder builder) {
        this.alpha = builder.alpha;
        this.beta = builder.beta;
        this.numIterations = builder.numIterations;
        this.numThreads = builder.numThreads;
        this.numTopics = builder.numTopics;
        this.threshold = builder.threshold;
        this.topicInferenceParams = builder.topicInferenceParams;
        this.coefOwn = builder.coefOwn;
        this.coefInLinks = builder.coefInLinks;
        this.coefOutLinks = builder.coefOutLinks;
        this.trainTweetsRatio = builder.trainTweetsRatio;
        this.interestsMapperParams = builder.interestsMapperParams;
    }

    public double getAlpha() {
        return alpha;
    }

    public double getBeta() {
        return beta;
    }

    public int getNumIterations() {
        return numIterations;
    }

    public int getNumThreads() {
        return numThreads;
    }

    public int getNumTopics() {
        return numTopics;
    }

    public double getThreshold() {
        return threshold;
    }

    public TopicInferenceParams getTopicInferenceParams() {
        return topicInferenceParams;
    }

    public double getCoefInLinks() {
        return coefInLinks;
    }

    public double getCoefOutLinks() {
        return coefOutLinks;
    }

    public double getCoefOwn() {
        return coefOwn;
    }

    public double getTrainTweetsRatio() {
        return trainTweetsRatio;
    }

    public InterestsMapperParams getInterestsMapperParams() {
        return interestsMapperParams;
    }

    public static class AlgorithmParamsBuilder {
        private double alpha = DEFAULT_ALGORITHM_PARAMS.alpha;
        private int numThreads = DEFAULT_ALGORITHM_PARAMS.numThreads;
        private int numIterations = DEFAULT_ALGORITHM_PARAMS.numIterations;
        private int numTopics = DEFAULT_ALGORITHM_PARAMS.numTopics;
        private double beta = DEFAULT_ALGORITHM_PARAMS.beta;
        private double threshold = DEFAULT_ALGORITHM_PARAMS.threshold;
        private TopicInferenceParams topicInferenceParams = DEFAULT_ALGORITHM_PARAMS.topicInferenceParams;
        private double coefOwn = DEFAULT_ALGORITHM_PARAMS.coefOwn;
        private double coefOutLinks = DEFAULT_ALGORITHM_PARAMS.coefOutLinks;
        private double coefInLinks = DEFAULT_ALGORITHM_PARAMS.coefInLinks;
        private double trainTweetsRatio = DEFAULT_ALGORITHM_PARAMS.trainTweetsRatio;
        public InterestsMapperParams interestsMapperParams = DEFAULT_ALGORITHM_PARAMS.interestsMapperParams;

        public AlgorithmParamsBuilder() {
        }

        public AlgorithmParamsBuilder(AlgorithmParams algorithmParams) {
            this.alpha = algorithmParams.alpha;
            this.numThreads = algorithmParams.numThreads;
            this.numIterations = algorithmParams.numIterations;
            this.numTopics = algorithmParams.numTopics;
            this.beta = algorithmParams.beta;
            this.threshold = algorithmParams.threshold;
            this.topicInferenceParams = algorithmParams.topicInferenceParams;
            this.coefOwn = algorithmParams.coefOwn;
            this.coefOutLinks = algorithmParams.coefOutLinks;
            this.coefInLinks = algorithmParams.coefInLinks;
            this.trainTweetsRatio = algorithmParams.trainTweetsRatio;
        }

        public AlgorithmParamsBuilder alpha(double alpha) {
            this.alpha = alpha;
            return this;
        }

        public AlgorithmParamsBuilder numThreads(int numThreads) {
            this.numThreads = numThreads;
            return this;
        }

        public AlgorithmParamsBuilder numIterations(int numIterations) {
            this.numIterations = numIterations;
            return this;
        }

        public AlgorithmParamsBuilder numTopics(int numTopics) {
            this.numTopics = numTopics;
            return this;
        }

        public AlgorithmParamsBuilder beta(double beta) {
            this.beta = beta;
            return this;
        }

        public AlgorithmParamsBuilder threshold(double threshold) {
            this.threshold = threshold;
            return this;
        }

        public AlgorithmParamsBuilder topicInferenceParams(TopicInferenceParams topicInferenceParams) {
            this.topicInferenceParams = topicInferenceParams;
            return this;
        }

        public AlgorithmParamsBuilder coefOwn(double coefOwn) {
            this.coefOwn = coefOwn;
            return this;
        }

        public AlgorithmParamsBuilder coefOutLinks(double coefOutLinks) {
            this.coefOutLinks = coefOutLinks;
            return this;
        }

        public AlgorithmParamsBuilder coefInLinks(double coefInLinks) {
            this.coefInLinks = coefInLinks;
            return this;
        }

        public AlgorithmParamsBuilder trainTweetsRatio(double trainTweetsRatio) {
            this.trainTweetsRatio = trainTweetsRatio;
            return this;
        }

        public AlgorithmParamsBuilder interestsMapperParams(InterestsMapperParams interestsMapperParams) {
            this.interestsMapperParams = interestsMapperParams;
            return this;
        }

        public AlgorithmParams build() {
            return new AlgorithmParams(this);
        }
    }
}
