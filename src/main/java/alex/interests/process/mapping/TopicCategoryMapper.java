package alex.interests.process.mapping;


import alex.interests.data.model.Interest;
import alex.interests.data.model.Topic;
import alex.interests.data.model.User;
import alex.interests.process.mapping.entities.Category;

import java.util.Set;
import java.util.SortedMap;

public interface TopicCategoryMapper {

	//void mapTopic(Topic topic);

	//void mapTopicsForUser(User user);

	SortedMap<Double, Category> mapTopicsToInterests(SortedMap<Double, Topic> topics);

}