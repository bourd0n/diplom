
package alex.interests.process.mapping.entities;

import com.fasterxml.jackson.annotation.*;
import com.google.common.base.MoreObjects;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.*;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Category implements Serializable {

    private int id;
    private String title;

    private List<Category> parentCategories = new ArrayList<>();
    private final Map<Integer, Category> childCategories = new HashMap<>();
    private Set<String> terms = new HashSet<>();
    private double ownWeight = 0.0;
    private boolean isTopCategory = false;

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("title", title)
                .add("top", isTopCategory)
                .add("ownWeight", ownWeight)
                .add("terms", terms)
                .toString();
    }

    public double getOwnWeight() {
        return ownWeight;
    }

    public Map<Integer, Category> getChildCategories() {
        return childCategories;
    }

    public void addOwnWeight(double ownWeight) {
        this.ownWeight += ownWeight;
    }

    public void addChildCategory(Category childCategory) {
        this.childCategories.putIfAbsent(childCategory.getId(), childCategory);
    }

    public Set<String> getTerms() {
        return terms;
    }

    public boolean isTopCategory() {
        return isTopCategory;
    }

    public void setTopCategory(boolean isTopCategory) {
        this.isTopCategory = isTopCategory;
    }

    @JsonIgnore
    public List<Category> getParentCategories() {
        return parentCategories;
    }

    @JsonProperty
    public void setParentCategories(List<Category> parentCategories) {
        this.parentCategories = parentCategories;
    }
}
