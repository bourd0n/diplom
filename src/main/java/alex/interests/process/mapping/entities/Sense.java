
package alex.interests.process.mapping.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Sense {

    private int id;
    private String title;
    private int linkDocCount;
    private int linkOccCount;
    private double priorProbability;
    private boolean fromTitle;
    private boolean fromRedirect;

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The linkDocCount
     */
    public int getLinkDocCount() {
        return linkDocCount;
    }

    /**
     * @param linkDocCount The linkDocCount
     */
    public void setLinkDocCount(int linkDocCount) {
        this.linkDocCount = linkDocCount;
    }

    /**
     * @return The linkOccCount
     */
    public int getLinkOccCount() {
        return linkOccCount;
    }

    /**
     * @param linkOccCount The linkOccCount
     */
    public void setLinkOccCount(int linkOccCount) {
        this.linkOccCount = linkOccCount;
    }

    /**
     * @return The priorProbability
     */
    public double getPriorProbability() {
        return priorProbability;
    }

    /**
     * @param priorProbability The priorProbability
     */
    public void setPriorProbability(double priorProbability) {
        this.priorProbability = priorProbability;
    }

    /**
     * @return The fromTitle
     */
    public boolean isFromTitle() {
        return fromTitle;
    }

    /**
     * @param fromTitle The fromTitle
     */
    public void setFromTitle(boolean fromTitle) {
        this.fromTitle = fromTitle;
    }

    /**
     * @return The fromRedirect
     */
    public boolean isFromRedirect() {
        return fromRedirect;
    }

    /**
     * @param fromRedirect The fromRedirect
     */
    public void setFromRedirect(boolean fromRedirect) {
        this.fromRedirect = fromRedirect;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
