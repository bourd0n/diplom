package alex.interests.process.mapping;

import alex.interests.data.model.Topic;
import alex.interests.process.mapping.entities.*;
import alex.interests.process.params.InterestsMapperParams;
import alex.interests.process.topic.lda.RemoveUselessWordsPipe;
import alex.interests.utils.ConfigProperties;
import alex.interests.utils.WordMap;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.languagetool.JLanguageTool;
import org.languagetool.MultiThreadedJLanguageTool;
import org.languagetool.language.AmericanEnglish;
import org.languagetool.language.English;
import org.languagetool.rules.RuleMatch;
import org.languagetool.rules.spelling.SpellingCheckRule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.concurrent.NotThreadSafe;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.*;
import java.util.stream.Collectors;

@NotThreadSafe
public class WikiTopicMapper implements TopicCategoryMapper {

    private static final Logger log = LoggerFactory.getLogger(WikiTopicMapper.class);
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private static final CloseableHttpClient httpClient = HttpClients.createDefault();

    private final Map<Integer, Category> allCategories = new HashMap<>();
    private final SetMultimap<Integer, Integer> categoryToSourceCategoryArticle = HashMultimap.create();
    private static final English english = new AmericanEnglish();

    private static final String sparse_terms_file = ConfigProperties.getInstance().getProperty("file.sparse.terms");
    private static final WordMap sparse_terms = new WordMap(WikiTopicMapper.class.getResource(sparse_terms_file).getPath());

    private final InterestsMapperParams params;

    public WikiTopicMapper(InterestsMapperParams params) {
        this.params = params;
    }

    @Override
    public SortedMap<Double, Category> mapTopicsToInterests(SortedMap<Double, Topic> topics) {
        int numTopTopics = params.getNumTopTopics();
        for (Map.Entry<Double, Topic> topicEntry : topics.entrySet()) {
            numTopTopics--;
            Double topicWeight = topicEntry.getKey();
            Topic topic = topicEntry.getValue();
            log.debug("Process topic {} with weight {}", topic, topicWeight);
            for (Map.Entry<Double, String> termEntry : topic.getTermsDistribution().entrySet()) {
                String term = termEntry.getValue();
                if (isSparseTerm(term)) {
                    continue;
                }
                final Double termWeight = termEntry.getKey();
                log.debug("Call search for term {} with weight {}", term, termWeight);
                SearchTermResult searchTermResult = searchTerm(term);
                if (CollectionUtils.isEmpty(searchTermResult.getLabels().get(0).getSenses())) {
                    //try to find by spell check
                    JLanguageTool langTool = new MultiThreadedJLanguageTool(english);
                    try {
                        List<RuleMatch> matches = langTool.check(term);
                        RuleMatch ruleMatch = matches.parallelStream()
                                .filter(rm -> rm.getRule() instanceof SpellingCheckRule).findFirst().orElse(null);
                        if (ruleMatch != null) {
                            final List<String> suggestedReplacements = ruleMatch.getSuggestedReplacements();
                            String spellCheckedReplacement = Iterables.getFirst(suggestedReplacements, null);
                            if (spellCheckedReplacement == null || suggestedReplacements.size() > 2) {
                                continue;
                            }
                            log.debug("Try to search term '{}' by spellchecked version '{}'", term, spellCheckedReplacement);
                            spellCheckedReplacement = URLEncoder.encode(spellCheckedReplacement, "UTF-8");
                            searchTermResult = searchTerm(spellCheckedReplacement);
                            term = spellCheckedReplacement;
                        } else {
                            continue;
                        }
                    } catch (IOException e) {
                        log.error("Exception from LangTool", e);
                    }
                }
                for (Label label : searchTermResult.getLabels()) {
                    String labelTerm = label.getText();
                    if (isSparseTerm(labelTerm)) {
                        continue;
                    }
                    if (label.isStopword()) {
                        log.debug("term '{}' is stopword in WM. Continue", labelTerm);
                        continue;
                    }
                    List<Sense> filteredSenses = filterSenses(label.getSenses(), labelTerm);
                    for (Sense sense : filteredSenses) {
                        Article article = exploreArticleById(sense.getId());
                        Category fakeCategoryArticle = allCategories.computeIfAbsent(sense.getId(), key -> articleToCategory(article));
                        fakeCategoryArticle.getTerms().add(labelTerm);
                        double senseProbability = sense.isFromTitle() || filteredSenses.size() == 1 ? 1.0 : sense.getPriorProbability();
                        double ownWeight = topicWeight * termWeight * senseProbability;
                        fakeCategoryArticle.addOwnWeight(ownWeight);
                        final List<ArticleCategory> articleCategories = article.getParentCategories();
                        if (CollectionUtils.isEmpty(articleCategories)) {
                            fakeCategoryArticle.setTopCategory(true);
                        }
                        for (ArticleCategory articleCategory : articleCategories) {
                            final int articleCategoryId = articleCategory.getId();
                            //get parent categories
                            getParentCategoriesRecursively(articleCategoryId, params.getParentCategoriesRecursiveLevel(), fakeCategoryArticle, sense.getId());
                            //final Category cachedArticleCategory = allCategories.get(articleCategoryId);
                            //double ownWeight = topicWeight * termWeight * sense.getPriorProbability();
                            //cachedArticleCategory.addOwnWeight(ownWeight);
                        }
                    }
                }
            }

            if (numTopTopics == 0)
                break;
        }
        log.debug("Finish processing topics");
        //calculate categories weights
        final SortedMap<Double, Category> categoriesWeights = new TreeMap<>(Comparator.reverseOrder());
        allCategories.values().stream().filter(Category::isTopCategory).forEach(category -> {
            Category categoryToPut = tryToReduceTopCategory(category);
            final Double sumCategoryWeight = sumWeight(categoryToPut);
            categoriesWeights.putIfAbsent(sumCategoryWeight, categoryToPut);
            //if (categoriesWeights.containsKey(sumCategoryWeight)) {
            //    if (!categoriesWeights.get(sumCategoryWeight).contains(categoryToPut)) {
            //        categoriesWeights.get(sumCategoryWeight).add(categoryToPut);
            //    }
            //} else {
            //    categoriesWeights.put(sumCategoryWeight, Sets.newHashSet(categoryToPut));
            //}
        });

        //SortedMap<Double, Category> finalCategoriesWeights = new TreeMap<>(Comparator.reverseOrder());
        //categoriesWeights.entrySet().stream().filter(doubleSetEntry -> doubleSetEntry.getValue().size() == 1).forEach(doubleSetEntry -> {
        //    finalCategoriesWeights.put(doubleSetEntry.getKey(), Iterables.getOnlyElement(doubleSetEntry.getValue()));
        //});
        final SortedMap<Double, Category> finalCategoriesWeights = new TreeMap<>(Comparator.reverseOrder());
        int returnedSize = (int) (categoriesWeights.size() * 0.8);
        for (Map.Entry<Double, Category> doubleCategoryEntry : categoriesWeights.entrySet()) {
            if (returnedSize <= 0)
                break;
            finalCategoriesWeights.put(doubleCategoryEntry.getKey(), doubleCategoryEntry.getValue());
            returnedSize--;
        }

        return finalCategoriesWeights;
    }

    private boolean isSparseTerm(String term) {
        return sparse_terms.containsKey(term);
    }

    private boolean isSparseCategory(Category category) {
        final String title = category.getTitle().toLowerCase();
        return title.contains("based in")
                || title.contains("by city")
                || title.contains("named after")
                || title.contains("by year")
                || title.contains("loanwords")
                || title.contains("by language")
                || title.contains("established in")
                || title.equals("terminology")
                || title.contains("words and phrases")
                || title.equals("Companies of the United States")
                || title.contains("by topic");
    }

    private Category articleToCategory(Article article) {
        Category category = new Category();
        category.setId(article.getId());
        category.setTitle(article.getTitle());
        return category;
    }

    //todo: need reduce if two child
    private Category tryToReduceTopCategory(Category category) {
        if (category.getOwnWeight() == 0.0) {
            final Set<Integer> sourceCategoryArticleIds = categoryToSourceCategoryArticle.get(category.getId());
            if (sourceCategoryArticleIds.size() == 1) {
                //only one source article category - reduce to it
                return allCategories.get(Iterables.getOnlyElement(sourceCategoryArticleIds));
            }
            final Map<Integer, Category> childCategories = category.getChildCategories();
            if (childCategories.size() == 1) {
                return tryToReduceTopCategory(Iterables.getOnlyElement(childCategories.values()));
            } else {
                Category childCategoryToReduce = null;
                Set<Integer> childCategoryToReduceSourceArticleCategories = null;
                for (Integer childCategoryId : childCategories.keySet()) {
                    final Set<Integer> childCategorySourceArticleCategories = categoryToSourceCategoryArticle.get(childCategoryId);
                    if (childCategorySourceArticleCategories.equals(sourceCategoryArticleIds)) {
                        childCategoryToReduce = allCategories.get(childCategoryId);
                        childCategoryToReduceSourceArticleCategories = childCategorySourceArticleCategories;
                        break;
                    }
                }
                if (childCategoryToReduce != null) {
                    //check that all siblings have child categories that subsets of all categories
                    for (Integer childCategoryId : childCategories.keySet()) {
                        final Set<Integer> childCategorySourceArticleCategories = categoryToSourceCategoryArticle.get(childCategoryId);
                        if (!childCategoryToReduceSourceArticleCategories.containsAll(childCategorySourceArticleCategories)) {
                            //cant reduce
                            childCategoryToReduce = null;
                            break;
                        }
                    }
                }
                if (childCategoryToReduce != null)
                    return tryToReduceTopCategory(childCategoryToReduce);
                else
                    return category;
            }
        } else {
            return category;
        }
    }

    private Double sumWeight(Category category) {
        final Set<Integer> sourceArticlesIds = categoryToSourceCategoryArticle.get(category.getId());
        if (sourceArticlesIds.isEmpty())
            return category.getOwnWeight();
        return sourceArticlesIds.stream().mapToDouble(id -> allCategories.get(id).getOwnWeight()).sum();
    }

    //todo: need to optimize - cache
    protected void getParentCategoriesRecursively(int categoryId, int recursiveLevel, Category childCategory, int sourceRootArticleId) {
        if (recursiveLevel != 0) {
            Category category = exploreCategoryById(categoryId);
            if (isSparseCategory(category))
                return;
            Category sourceCategoryFromMap = allCategories.computeIfAbsent(category.getId(), key -> category);
            categoryToSourceCategoryArticle.put(category.getId(), sourceRootArticleId);
            if (childCategory != null) {
                sourceCategoryFromMap.addChildCategory(childCategory);
            }
            int finalRecursiveLevel = recursiveLevel - 1;
            if (finalRecursiveLevel != 0) {
                for (Category parentCategory : category.getParentCategories()) {
                    getParentCategoriesRecursively(parentCategory.getId(), finalRecursiveLevel, sourceCategoryFromMap, sourceRootArticleId);
                }

                boolean isParentMarkTop = false;
                if (finalRecursiveLevel == 1) {
                    for (Category parentCategory : category.getParentCategories()) {
                        final Category parentCategoryFromMap = allCategories.get(parentCategory.getId());
                        if (parentCategoryFromMap != null && parentCategoryFromMap.isTopCategory()) {
                            isParentMarkTop = true;
                            break;
                        }
                    }
                }

                if (!isParentMarkTop) {
                    sourceCategoryFromMap.setTopCategory(true);
                }
            } else {
                sourceCategoryFromMap.setTopCategory(true);
            }
        }
    }

    private List<Sense> filterSenses(List<Sense> senses, String term) {
        List<Sense> sensesFromTitle = senses.parallelStream().filter(Sense::isFromTitle).collect(Collectors.toList());
        if (!sensesFromTitle.isEmpty() && !term.equals("apple"))
            return sensesFromTitle;
        List<Sense> commonSenses = findCommonSenses(senses, term);
        if (!commonSenses.isEmpty())
            return commonSenses;
        return senses
                .parallelStream()
                .filter(sense -> sense.getPriorProbability() >= params.getThresholdSenses())
                .collect(Collectors.toList());
    }

    private List<Sense> findCommonSenses(List<Sense> senses, String term) {
        String senseName = null;
        switch (term.toLowerCase()) {
            case "tesla":
                senseName = "Tesla Motors";
                break;
        }
        final String finalSenseName = senseName;
        return senses
                .parallelStream()
                .filter(sense -> sense.getTitle().equals(finalSenseName))
                .collect(Collectors.toList());
    }

    protected String getQueryForTermSearch(String term) {
        return "http://wikipedia-miner.cms.waikato.ac.nz/services/search?query=" + term + "&responseFormat=json&complex=true";
    }

    protected String getQueryForExploreArticleById(int id) {
        return "http://wikipedia-miner.cms.waikato.ac.nz/services/exploreArticle?id=" + id
                + "&parentCategories=true&responseFormat=json";
    }

    protected String getQueryForExploreCategoryById(int id) {
        return "http://wikipedia-miner.cms.waikato.ac.nz/services/exploreCategory?id=" + id
                + "&parentCategories=true&responseFormat=json";
    }

    protected SearchTermResult searchTerm(String term) {
        //know gaps
        if (term.equalsIgnoreCase("ios")) {
            return executeRequest(getQueryForTermSearch("iOS"), SearchTermResult.class);
        }
        return executeRequest(getQueryForTermSearch(term), SearchTermResult.class);
    }

    protected Article exploreArticleById(int id) {
        return executeRequest(getQueryForExploreArticleById(id), Article.class);
    }

    protected Category exploreCategoryById(int id) {
        return executeRequest(getQueryForExploreCategoryById(id), Category.class);
    }

    protected <T> T executeRequest(String request, Class<T> clazz) {
        final HttpGet httpGet = new HttpGet(request);
        try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
            HttpEntity entity = response.getEntity();
            String jsonStr = EntityUtils.toString(entity);
            return objectMapper.readValue(jsonStr, clazz);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void finalize() throws Throwable {
        httpClient.close();
        super.finalize();
    }
}