package alex.interests.process.topic.lda;

import cc.mallet.pipe.Pipe;
import cc.mallet.types.Instance;

import java.io.Serializable;

public class DeletePunctuationPipe extends Pipe implements Serializable {

    public Instance pipe(Instance carrier) {
        if (carrier.getData() instanceof String) {
            String data = (String) carrier.getData();
            carrier.setData(TweetPreprocessor.deletePunctuation(data));
        } else {
            throw new IllegalArgumentException("DeletePunctuationPipe expects a String, found a " + carrier.getData().getClass());
        }
        return carrier;
    }
}
