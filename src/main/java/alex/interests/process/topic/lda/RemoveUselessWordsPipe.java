package alex.interests.process.topic.lda;

import alex.interests.utils.ConfigProperties;
import alex.interests.utils.WordMap;
import cc.mallet.pipe.Pipe;
import cc.mallet.types.FeatureSequenceWithBigrams;
import cc.mallet.types.Instance;
import cc.mallet.types.Token;
import cc.mallet.types.TokenSequence;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//copy from mallet TokenSequenceRemoveStopwords
public class RemoveUselessWordsPipe extends Pipe implements Serializable {

    private static final Pattern uselessWordPattern = Pattern.compile("(?:<[^>]+>)|(pic\\.twitter\\.com/[\\w]+)|((https?|ftp|file|htttps?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|])");
    private static final String abbreviations_remove_file = ConfigProperties.getInstance().getProperty("file.abbreviations.remove");
    private static final String abbreviations_replace_file = ConfigProperties.getInstance().getProperty("file.abbreviations.replace");
    private static final WordMap abbreviations_remove =
            new WordMap(RemoveUselessWordsPipe.class.getResource(abbreviations_remove_file).getPath(), ":");
    private static final WordMap abbreviations_replace =
            new WordMap(RemoveUselessWordsPipe.class.getResource(abbreviations_replace_file).getPath(), ":");

    // xxx Use a gnu.trove collection instead
    HashSet<String> stoplist = null;
    boolean caseSensitive = true;
    boolean markDeletions = false;

    private HashSet<String> newDefaultStopList() {
        HashSet<String> sl = new HashSet<>();
        Collections.addAll(sl, stopwords);
        return sl;
    }

    /**
     * Load a stoplist from a file.
     *
     * @param stoplistFile   The file to load
     * @param encoding       The encoding of the stoplist file (eg UTF-8)
     * @param includeDefault Whether to include the standard mallet English stoplist
     */
    public RemoveUselessWordsPipe(File stoplistFile, String encoding, boolean includeDefault,
                                  boolean caseSensitive, boolean markDeletions) {
        if (!includeDefault) {
            stoplist = new HashSet<>();
        } else {
            stoplist = newDefaultStopList();
        }

        addStopWords(fileToStringArray(stoplistFile, encoding));

        this.caseSensitive = caseSensitive;
        this.markDeletions = markDeletions;
    }

    public RemoveUselessWordsPipe addStopWords(String[] words) {
        Collections.addAll(stoplist, words);
        return this;
    }


    public RemoveUselessWordsPipe removeStopWords(String[] words) {
        for (String word : words)
            stoplist.remove(word);
        return this;
    }


    private String[] fileToStringArray(File f, String encoding) {
        ArrayList<String> wordarray = new ArrayList<>();

        try {

            BufferedReader input;
            if (encoding == null) {
                input = new BufferedReader(new FileReader(f));
            } else {
                input = new BufferedReader(new InputStreamReader(new FileInputStream(f), encoding));
            }
            String line;

            while ((line = input.readLine()) != null) {
                String[] words = line.split("\\s+");
                Collections.addAll(wordarray, words);
            }

        } catch (IOException e) {
            throw new IllegalArgumentException("Trouble reading file " + f);
        }
        return wordarray.toArray(new String[wordarray.size()]);
    }

    public Instance pipe(Instance carrier) {
        TokenSequence ts = (TokenSequence) carrier.getData();
        // xxx This doesn't seem so efficient.  Perhaps have TokenSequence
        // use a LinkedList, and remove Tokens from it? -?
        // But a LinkedList implementation of TokenSequence would be quite inefficient -AKM
        TokenSequence ret = new TokenSequence();
        Token prevToken = null;
        for (Token t : ts) {
            final String tokenText = caseSensitive ? t.getText() : t.getText().toLowerCase();
            final Matcher matcher = uselessWordPattern.matcher(tokenText);
            if (tokenText.length() >= 3 && !stoplist.contains(tokenText) && !abbreviations_remove.containsKey(tokenText) && !matcher.matches()) {
                // xxx Should we instead make and add a copy of the Token?
                if (abbreviations_replace.containsKey(tokenText)) {
                    ret.add(new Token(abbreviations_replace.find(tokenText)));
                } else {
                    ret.add(t);
                }
                prevToken = t;
            } else if (markDeletions && prevToken != null)
                prevToken.setProperty(FeatureSequenceWithBigrams.deletionMark, t.getText());
        }
        carrier.setData(ret);
        return carrier;
    }

    // Serialization

    private static final long serialVersionUID = 1;
    private static final int CURRENT_SERIAL_VERSION = 2;

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.writeInt(CURRENT_SERIAL_VERSION);
        out.writeBoolean(caseSensitive);
        out.writeBoolean(markDeletions);
        out.writeObject(stoplist); // New as of CURRENT_SERIAL_VERSION 2
    }

    @SuppressWarnings("unchecked")
    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        int version = in.readInt();
        caseSensitive = in.readBoolean();
        if (version > 0)
            markDeletions = in.readBoolean();
        if (version > 1) {
            stoplist = (HashSet<String>) in.readObject();
        }

    }


    static final String[] stopwords =
            {
                    "a",
                    "able",
                    "about",
                    "above",
                    "according",
                    "accordingly",
                    "across",
                    "actually",
                    "after",
                    "afterwards",
                    "again",
                    "against",
                    "all",
                    "allow",
                    "allows",
                    "almost",
                    "alone",
                    "along",
                    "already",
                    "also",
                    "although",
                    "always",
                    "am",
                    "among",
                    "amongst",
                    "an",
                    "and",
                    "another",
                    "any",
                    "anybody",
                    "anyhow",
                    "anyone",
                    "anything",
                    "anyway",
                    "anyways",
                    "anywhere",
                    "apart",
                    "appear",
                    "appreciate",
                    "appropriate",
                    "are",
                    "around",
                    "as",
                    "aside",
                    "ask",
                    "asking",
                    "associated",
                    "at",
                    "available",
                    "away",
                    "awfully",
                    "b",
                    "be",
                    "became",
                    "because",
                    "become",
                    "becomes",
                    "becoming",
                    "been",
                    "before",
                    "beforehand",
                    "behind",
                    "being",
                    "believe",
                    "below",
                    "beside",
                    "besides",
                    "best",
                    "better",
                    "between",
                    "beyond",
                    "both",
                    "brief",
                    "but",
                    "by",
                    "c",
                    "came",
                    "can",
                    "cannot",
                    "cant",
                    "cause",
                    "causes",
                    "certain",
                    "certainly",
                    "changes",
                    "clearly",
                    "co",
                    "com",
                    "come",
                    "comes",
                    "concerning",
                    "consequently",
                    "consider",
                    "considering",
                    "contain",
                    "containing",
                    "contains",
                    "corresponding",
                    "could",
                    "course",
                    "currently",
                    "d",
                    "definitely",
                    "described",
                    "despite",
                    "did",
                    "different",
                    "do",
                    "does",
                    "doing",
                    "done",
                    "down",
                    "downwards",
                    "during",
                    "e",
                    "each",
                    "edu",
                    "eg",
                    "eight",
                    "either",
                    "else",
                    "elsewhere",
                    "enough",
                    "entirely",
                    "especially",
                    "et",
                    "etc",
                    "even",
                    "ever",
                    "every",
                    "everybody",
                    "everyone",
                    "everything",
                    "everywhere",
                    "ex",
                    "exactly",
                    "example",
                    "except",
                    "f",
                    "far",
                    "few",
                    "fifth",
                    "first",
                    "five",
                    "followed",
                    "following",
                    "follows",
                    "for",
                    "former",
                    "formerly",
                    "forth",
                    "four",
                    "from",
                    "further",
                    "furthermore",
                    "g",
                    "get",
                    "gets",
                    "getting",
                    "given",
                    "gives",
                    "go",
                    "goes",
                    "going",
                    "gone",
                    "got",
                    "gotten",
                    "greetings",
                    "h",
                    "had",
                    "happens",
                    "hardly",
                    "has",
                    "have",
                    "having",
                    "he",
                    "hello",
                    "help",
                    "hence",
                    "her",
                    "here",
                    "hereafter",
                    "hereby",
                    "herein",
                    "hereupon",
                    "hers",
                    "herself",
                    "hi",
                    "him",
                    "himself",
                    "his",
                    "hither",
                    "hopefully",
                    "how",
                    "howbeit",
                    "however",
                    "i",
                    "ie",
                    "if",
                    "ignored",
                    "immediate",
                    "in",
                    "inasmuch",
                    "inc",
                    "indeed",
                    "indicate",
                    "indicated",
                    "indicates",
                    "inner",
                    "insofar",
                    "instead",
                    "into",
                    "inward",
                    "is",
                    "it",
                    "its",
                    "itself",
                    "j",
                    "just",
                    "k",
                    "keep",
                    "keeps",
                    "kept",
                    "know",
                    "knows",
                    "known",
                    "l",
                    "last",
                    "lately",
                    "later",
                    "latter",
                    "latterly",
                    "least",
                    "less",
                    "lest",
                    "let",
                    "like",
                    "liked",
                    "likely",
                    "little",
                    "look",
                    "looking",
                    "looks",
                    "ltd",
                    "m",
                    "mainly",
                    "many",
                    "may",
                    "maybe",
                    "me",
                    "mean",
                    "meanwhile",
                    "merely",
                    "might",
                    "more",
                    "moreover",
                    "most",
                    "mostly",
                    "much",
                    "must",
                    "my",
                    "myself",
                    "n",
                    "name",
                    "namely",
                    "nd",
                    "near",
                    "nearly",
                    "necessary",
                    "need",
                    "needs",
                    "neither",
                    "never",
                    "nevertheless",
                    "new",
                    "next",
                    "nine",
                    "no",
                    "nobody",
                    "non",
                    "none",
                    "noone",
                    "nor",
                    "normally",
                    "not",
                    "nothing",
                    "novel",
                    "now",
                    "nowhere",
                    "o",
                    "obviously",
                    "of",
                    "off",
                    "often",
                    "oh",
                    "ok",
                    "okay",
                    "old",
                    "on",
                    "once",
                    "one",
                    "ones",
                    "only",
                    "onto",
                    "or",
                    "other",
                    "others",
                    "otherwise",
                    "ought",
                    "our",
                    "ours",
                    "ourselves",
                    "out",
                    "outside",
                    "over",
                    "overall",
                    "own",
                    "p",
                    "particular",
                    "particularly",
                    "per",
                    "perhaps",
                    "placed",
                    "please",
                    "plus",
                    "possible",
                    "presumably",
                    "probably",
                    "provides",
                    "q",
                    "que",
                    "quite",
                    "qv",
                    "r",
                    "rather",
                    "rd",
                    "re",
                    "really",
                    "reasonably",
                    "regarding",
                    "regardless",
                    "regards",
                    "relatively",
                    "respectively",
                    "right",
                    "s",
                    "said",
                    "same",
                    "saw",
                    "say",
                    "saying",
                    "says",
                    "second",
                    "secondly",
                    "see",
                    "seeing",
                    "seem",
                    "seemed",
                    "seeming",
                    "seems",
                    "seen",
                    "self",
                    "selves",
                    "sensible",
                    "sent",
                    "serious",
                    "seriously",
                    "seven",
                    "several",
                    "shall",
                    "she",
                    "should",
                    "since",
                    "six",
                    "so",
                    "some",
                    "somebody",
                    "somehow",
                    "someone",
                    "something",
                    "sometime",
                    "sometimes",
                    "somewhat",
                    "somewhere",
                    "soon",
                    "sorry",
                    "specified",
                    "specify",
                    "specifying",
                    "still",
                    "sub",
                    "such",
                    "sup",
                    "sure",
                    "t",
                    "take",
                    "taken",
                    "tell",
                    "tends",
                    "th",
                    "than",
                    "thank",
                    "thanks",
                    "thanx",
                    "that",
                    "thats",
                    "the",
                    "their",
                    "theirs",
                    "them",
                    "themselves",
                    "then",
                    "thence",
                    "there",
                    "thereafter",
                    "thereby",
                    "therefore",
                    "therein",
                    "theres",
                    "thereupon",
                    "these",
                    "they",
                    "think",
                    "third",
                    "this",
                    "thorough",
                    "thoroughly",
                    "those",
                    "though",
                    "three",
                    "through",
                    "throughout",
                    "thru",
                    "thus",
                    "to",
                    "together",
                    "too",
                    "took",
                    "toward",
                    "towards",
                    "tried",
                    "tries",
                    "truly",
                    "try",
                    "trying",
                    "twice",
                    "two",
                    "u",
                    "un",
                    "under",
                    "unfortunately",
                    "unless",
                    "unlikely",
                    "until",
                    "unto",
                    "up",
                    "upon",
                    "us",
                    "use",
                    "used",
                    "useful",
                    "uses",
                    "using",
                    "usually",
                    "uucp",
                    "v",
                    "value",
                    "various",
                    "very",
                    "via",
                    "viz",
                    "vs",
                    "w",
                    "want",
                    "wants",
                    "was",
                    "way",
                    "we",
                    "welcome",
                    "well",
                    "went",
                    "were",
                    "what",
                    "whatever",
                    "when",
                    "whence",
                    "whenever",
                    "where",
                    "whereafter",
                    "whereas",
                    "whereby",
                    "wherein",
                    "whereupon",
                    "wherever",
                    "whether",
                    "which",
                    "while",
                    "whither",
                    "who",
                    "whoever",
                    "whole",
                    "whom",
                    "whose",
                    "why",
                    "will",
                    "willing",
                    "wish",
                    "with",
                    "within",
                    "without",
                    "wonder",
                    "would",
                    "would",
                    "x",
                    "y",
                    "yes",
                    "yet",
                    "you",
                    "your",
                    "yours",
                    "yourself",
                    "yourselves",
                    "z",
                    "zero",
                    // stop words for paper abstracts
                    "abstract",
                    "paper",
                    "presents",
                    "discuss",
                    "discusses",
                    "conclude",
                    "concludes",
                    "based",
                    //"approach"
            };


}
