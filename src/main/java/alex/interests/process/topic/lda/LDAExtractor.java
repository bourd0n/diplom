package alex.interests.process.topic.lda;

import alex.interests.data.model.Topic;
import alex.interests.data.model.Tweet;
import alex.interests.data.model.User;
import alex.interests.process.params.AlgorithmParams;
import alex.interests.process.params.TopicInferenceParams;
import alex.interests.process.topic.TopicExtractor;
import alex.interests.process.topic.TopicModel;
import cc.mallet.pipe.iterator.ArrayIterator;
import cc.mallet.topics.ParallelTopicModel;
import cc.mallet.topics.TopicInferencer;
import cc.mallet.types.Alphabet;
import cc.mallet.types.IDSorter;
import cc.mallet.types.Instance;
import cc.mallet.types.InstanceList;
import org.apache.commons.lang.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.concurrent.ThreadSafe;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@ThreadSafe
public class LDAExtractor implements TopicExtractor {
    private static final Logger log = LoggerFactory.getLogger(LDAExtractor.class);

    private final AlgorithmParams algorithmParams;
    private static final ConcurrentMap<String, String> locksForTopicCalculation = new ConcurrentHashMap<String, String>();

    public LDAExtractor(AlgorithmParams algorithmParams) {
        this.algorithmParams = algorithmParams;
    }

    @Override
    public TopicModel extractUserOwnTopics(User user) {
        if (user.isTopicsCalculated())
            return user.getOwnTopicModel();
        //to not allow multiple calculation for same user
        synchronized (getSyncObject(user.getId())) {
            if (user.isTopicsCalculated())
                return user.getOwnTopicModel();
            List<Tweet> tweets = user.getTweets(true);
            log.debug("Run LDA for {}", user);
            TopicModel topicModel = runLDA(tweets);
            user.setOwnTopicModel(topicModel);
            return topicModel;
        }
    }

    private Object getSyncObject(final String id) {
        locksForTopicCalculation.putIfAbsent(id, id);
        return locksForTopicCalculation.get(id);
    }

    private TopicModel runLDA(List<Tweet> tweets) {
        try {
            int numIterations = algorithmParams.getNumIterations();
            int numThreads = algorithmParams.getNumThreads();
            double alpha = algorithmParams.getAlpha();
            double beta = algorithmParams.getBeta();
            int numTopics = algorithmParams.getNumTopics();
            TopicInferenceParams topicInferenceParams = algorithmParams.getTopicInferenceParams();
            InstanceList instances = LDAHelper.thruTrainPipe(new ArrayIterator(tweets));
            ParallelTopicModel model = new ParallelTopicModel(numTopics, alpha, beta);
            model.addInstances(instances);
            // Use numThreads parallel samplers, which each look at one half the corpus and combine
            // statistics after every iteration.
            model.setNumThreads(numThreads);
            // Run the model for numIterations iterations and stop
            model.setNumIterations(numIterations);
            log.debug("Start model estimate for {} tweets", tweets.size());
            model.estimate();
            log.debug("Finish model estimate for {} tweets", tweets.size());
            Alphabet dataAlphabet = instances.getDataAlphabet();
            ArrayList<TreeSet<IDSorter>> topicSortedWords = model.getSortedWords();
            List<Topic> topics = new ArrayList<>(numTopics);
            for (int topic = 0; topic < numTopics; topic++) {
                SortedMap<Double, String> termsDistribution = new TreeMap<>();
                Iterator<IDSorter> iterator = topicSortedWords.get(topic).iterator();
                int rank = 0;
                while (iterator.hasNext() && rank < 10) {
                    IDSorter idCountPair = iterator.next();
                    String term = (String) dataAlphabet.lookupObject(idCountPair.getID());
                    Double weight = idCountPair.getWeight();
                    termsDistribution.put(weight, term);
                    rank++;
                }
                topics.add(new Topic(termsDistribution));
            }

            Instance instanceOneLine = LDAHelper.allTweetsToOneInstance(tweets);
            TopicInferencer inferencer = model.getInferencer();
            log.debug("Start inference for {} tweets", tweets.size());
            double[] sampledDistribution = inferencer.getSampledDistribution(instanceOneLine,
                    topicInferenceParams.numIterations,
                    topicInferenceParams.thinning,
                    topicInferenceParams.burnIn);
            log.debug("Finish inference for {} tweets", tweets.size());
            List<Double> topicsDistribution = Arrays.asList(ArrayUtils.toObject(sampledDistribution));
            TopicModel topicModel = new TopicModel(topics, topicsDistribution, model);
            log.debug("LDA return own user {}", topicModel);
            return topicModel;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Topic> extractUserFriendsTopics(User user) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Topic> extractUserFollowersTopics(User user) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Topic> extractUserRetweetedTopics(User user) {
        throw new UnsupportedOperationException();
    }

    @Override
    public TopicModel extractTopics(List<Tweet> tweets) {
        return runLDA(tweets);
    }
}