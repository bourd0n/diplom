package alex.interests.process.topic.lda;

import alex.interests.data.model.Tweet;
import alex.interests.utils.ConfigProperties;
import cc.mallet.pipe.*;
import cc.mallet.pipe.iterator.StringArrayIterator;
import cc.mallet.types.Instance;
import cc.mallet.types.InstanceList;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

public class LDAHelper {
    //links and html tags will be removed
    private static final Pattern pattern = Pattern.compile("(?:<[^>]+>)|(pic\\.twitter\\.com/[\\w]+)|((https?|ftp|file|htttps?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|])|(\\p{L}+[#$\\*\\-@]*[\\p{L}\\d]+)|(\\p{L})");

    public static Pipe getPipe(boolean forTrain) {
        // Begin by importing documents from text to feature sequences
        // Pipes: lowercase, tokenize, remove stopwords, map to features
        List<Pipe> pipeList = new ArrayList<>();
        pipeList.add(new ExtractTextFromTweetPipe());
        pipeList.add(new CharSequenceLowercase());
        pipeList.add(new CharSequence2TokenSequence(pattern));//aa1a, aaa, aa11
        File stopwordsFile = new File(LDAHelper.class.getResource(ConfigProperties.getInstance().getProperty("file.stoplist")).getFile());
        pipeList.add(new RemoveUselessWordsPipe(stopwordsFile, "UTF-8", true, false, false));
        if (forTrain) {
            pipeList.add(new TokenSequence2FeatureSequence());
        }
        return new SerialPipes(pipeList);
    }

    public static InstanceList thruTrainPipe(Iterator<Instance> iteratorSupplier) {
        InstanceList instances = new InstanceList(getPipe(true));
        instances.addThruPipe(iteratorSupplier);
        return instances;
    }

    public static InstanceList thruTestPipe(Iterator<Instance> iteratorSupplier) {
        InstanceList instances = new InstanceList(getPipe(false));
        instances.addThruPipe(iteratorSupplier);
        return instances;
    }

    public static Instance allTweetsToOneInstance(Collection<Tweet> tweets) {
        StringBuilder allTweetsToOneStringBuilder = new StringBuilder();
        tweets.stream().forEachOrdered(tweet -> allTweetsToOneStringBuilder.append(tweet.getText()).append(" "));
        String allTweetsInOneString = allTweetsToOneStringBuilder.toString();
        InstanceList oneInstanceList = thruTrainPipe(new StringArrayIterator(new String[]{allTweetsInOneString}));
        return oneInstanceList.get(0);
    }


}
