package alex.interests.process.topic;

import alex.interests.data.model.Topic;
import cc.mallet.topics.ParallelTopicModel;
import cc.mallet.topics.TopicInferencer;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.*;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.*;

@JsonIgnoreProperties({"parallelTopicModel"})
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
public class TopicModel implements Serializable {
    private final List<Topic> topics;
    private final List<Double> topicsDistribution;
    private final ParallelTopicModel parallelTopicModel;

    private TopicModel(){
        topics = null;
        topicsDistribution = null;
        parallelTopicModel = null;
    }

    public TopicModel(List<Topic> topics, List<Double> topicsDistribution, ParallelTopicModel parallelTopicModel) {
        this.topics = topics;
        this.topicsDistribution = topicsDistribution;
        this.parallelTopicModel = parallelTopicModel;
    }

    public List<Topic> getTopics() {
        return Collections.unmodifiableList(topics);
    }

    public List<Double> getTopicsDistribution() {
        return Collections.unmodifiableList(topicsDistribution);
    }

    public TopicInferencer getTopicInferencer() {
        return parallelTopicModel.getInferencer();
    }

    public ParallelTopicModel getParallelTopicModel() {
        return parallelTopicModel;
    }

    @Override
    public String toString() {
        return toStringWithDistribution(topicsDistribution);
    }

    public String toStringWithDistribution(List<Double> topicsDistribution) {
        StringBuilder stringBuilder = new StringBuilder("TopicModel : \n");
        DecimalFormat df = new DecimalFormat("#.#####");
        for (int i = 0; i < topicsDistribution.size(); i++) {
            stringBuilder.append(df.format(topicsDistribution.get(i))).append("\t").append(topics.get(i)).append("\n");
        }
        return stringBuilder.toString();
    }

    public String toStringSortedByDistribution() {
        SortedMap<Double, Topic> sortedMap = new TreeMap<>(Comparator.reverseOrder());
        for (int i = 0; i < topicsDistribution.size(); i++) {
            sortedMap.put(topicsDistribution.get(i), topics.get(i));
        }
        return toStringSortedByDistribution(sortedMap);
    }

    public static String toStringSortedByDistribution(SortedMap<Double, Topic> sortedMap) {
        StringBuilder stringBuilder = new StringBuilder("TopicModel[sorted] : \n");
        DecimalFormat df = new DecimalFormat("#.#####");
        for (Map.Entry<Double, Topic> entry : sortedMap.entrySet()) {
            stringBuilder.append(df.format(entry.getKey()))
                    .append("\t")
                    .append(entry.getValue())
                    .append("\n");
        }
        return stringBuilder.toString();
    }

    //Serialization
    public void writeModel(String modelFileName, String distributionsFileName) {
        try {
            parallelTopicModel.write(new File(modelFileName));
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
            objectMapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, true);
            objectMapper.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false);
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            FileWriter distributionsFileWriter = new FileWriter(distributionsFileName);
            objectMapper.writeValue(distributionsFileWriter, this);
            distributionsFileWriter.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static TopicModel readModel(String modelFileName, String distributionsFileName) {
        try {
            final ParallelTopicModel parallelTopicModel = ParallelTopicModel.read(new File(modelFileName));
            ObjectMapper objectMapper = new ObjectMapper();
            final TopicModel topicModel = objectMapper.readValue(new File(distributionsFileName), TopicModel.class);
            return new TopicModel(topicModel.getTopics(), topicModel.getTopicsDistribution(), parallelTopicModel);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
