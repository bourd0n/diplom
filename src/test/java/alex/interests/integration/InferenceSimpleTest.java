package alex.interests.integration;

import alex.interests.data.extractor.MongoExtractor;
import alex.interests.data.model.Topic;
import alex.interests.data.model.Tweet;
import alex.interests.data.model.User;
import alex.interests.process.UserInterestsInferencer;
import alex.interests.process.params.AlgorithmParams;
import alex.interests.process.params.ReferenceType;
import alex.interests.process.params.RelationType;
import alex.interests.test.DataExtractorWrapper;
import alex.interests.test.PerplexityUtils;
import alex.interests.test.UserWrapper;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.type.MapLikeType;
import org.junit.Test;

import java.io.File;
import java.util.List;
import java.util.SortedMap;

public class InferenceSimpleTest {

    @Test
    public void inferenceTopicWithRegularizationAndPerplexityTesting() throws Exception {
        File file = new File("I:\\programming\\diplom\\diplom\\logs\\topic_distribution_620601589.json");
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        objectMapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, true);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        final MapLikeType sortedMapType = objectMapper.getTypeFactory().constructMapLikeType(SortedMap.class, Double.class, Topic.class);

        AlgorithmParams algorithmParams = new AlgorithmParams.AlgorithmParamsBuilder()
                .numIterations(2000)
                .numThreads(4)
                .build();
        final MongoExtractor mongoExtractor = MongoExtractor.getInstance();
        DataExtractorWrapper dataExtractorWrapper = new DataExtractorWrapper(mongoExtractor, algorithmParams);
        User user = dataExtractorWrapper.getUserById("620601589");

        SortedMap<Double, Topic> calculatedTopics;
        if (!file.exists()) {
            UserInterestsInferencer interestsInferencer = new UserInterestsInferencer(mongoExtractor, algorithmParams);
            calculatedTopics = interestsInferencer.inferenceTopics(user,
                    RelationType.FOLLOWING, ReferenceType.ALL);
            file.createNewFile();
            objectMapper.writeValue(file, calculatedTopics);
        } else {
            calculatedTopics = objectMapper.readValue(file, sortedMapType);
        }
        UserWrapper userWrapper = (UserWrapper) user;
        final List<Tweet> tweetsForTest = userWrapper.getTweetsForTest(true);
        PerplexityUtils.calculatePerplexity(tweetsForTest, calculatedTopics);
    }

    //@Test
    //public void testObjectMapper() throws IOException {
    //    File file = new File("D:\\MY\\diplom\\diplom\\logs\\topic_distribution.json");
    //    ObjectMapper objectMapper = new ObjectMapper();
    //    objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
    //    objectMapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, true);
    //    objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    //    final MapLikeType mapLikeType = objectMapper.getTypeFactory().constructMapLikeType(SortedMap.class, Double.class, Topic.class);
    //
    //    if (!file.createNewFile()){
    //        //final MapLikeType mapLikeType = objectMapper.getTypeFactory().constructMapLikeType(SortedMap.class, Double.class, Topic.class);
    //        final SortedMap<Double, Topic> sortedMap = objectMapper.readValue(file, mapLikeType);
    //        System.out.println("Read success");
    //        System.out.println(sortedMap);
    //    } else {
    //        SortedMap<Double, Topic> sortedMap = new TreeMap<>(Comparator.reverseOrder());
    //        SortedMap<Double, String> stringSortedMap = new TreeMap<>(Comparator.reverseOrder());
    //        stringSortedMap.put(1.0, "aadasd");
    //        stringSortedMap.put(2.0, "asd");
    //        Topic topic1 = new Topic("1", stringSortedMap);
    //        Topic topic2 = new Topic("2", stringSortedMap);
    //        sortedMap.put(1.4, topic1);
    //        sortedMap.put(2.0, topic2);
    //        objectMapper.writeValue(file, sortedMap);
    //        System.out.println("Write success");
    //    }
    //
    //}

}
