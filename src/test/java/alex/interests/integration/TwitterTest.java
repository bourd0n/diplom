package alex.interests.integration;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.*;
import twitter4j.auth.OAuth2Token;
import twitter4j.conf.ConfigurationBuilder;

import java.util.*;

public class TwitterTest {

    private static final Logger log = LoggerFactory.getLogger(TwitterTest.class);

    @Test
    public void testTwitter() throws TwitterException {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                //.setOAuthConsumerKey("j360GiXDDSphpAs1iTYV6PuZv")
                //.setOAuthConsumerSecret("EmVcIIceqG1E8xBNOqf7KszjtZPr1uCZXNZ737xz6ZiSWWS8qL")
                .setOAuthConsumerKey("ByBDfW7QEguZRu2X8hvzk5Cz6")
                .setOAuthConsumerSecret("sOZHmwWxjJy02oCHViVZTaWfu41a9C3X5VoLAcsC2L7QyajeA5")
                        //.setOAuthAccessToken("1648547509-DWD44PYBzxFKiHkgNiEwCz1PvQ8HnlC9cTN7b9M")
                        //.setOAuthAccessTokenSecret("9Kx17bpP6JNB3WUK79LbKxTnmDHEVOm6FVMIw5feQhMqe")
                .setApplicationOnlyAuthEnabled(true);
        TwitterFactory tf = new TwitterFactory(cb.build());
        Twitter twitter = tf.getInstance();
        OAuth2Token token = twitter.getOAuth2Token();

        int pageno = 1;
        String user = "wylsacom";
        List<Status> statuses = new ArrayList<>();

        System.out.println(twitter.getRateLimitStatus());

        while (true) {

            try {

                int size = statuses.size();
                Paging page = new Paging(pageno++, 200);
                statuses.addAll(twitter.getUserTimeline(user, page));
                if (statuses.size() == size)
                    break;
            } catch (TwitterException e) {
                e.printStackTrace();
            }
        }

        for (Status status : statuses) {
            if (status.getRetweetCount() > 5) {
                Set<Long> retweetedUsers = getRetweetedUsers(twitter, status.getId());
                log.debug("RetweetedUsers size: StatusId {} retweetedCount {} retweetedUsersSize {}", status.getId(), status.getRetweetCount(), retweetedUsers.size());
            }
        }

        System.out.println("Total: " + statuses.size());
    }

    private Set<Long> getRetweetedUsers(Twitter twitter, long statusId) throws TwitterException {
        Set<Long> idsSet = new HashSet<>();
        long cursor = -1;
        IDs ids;
        log.debug("Listing following ids.");
        do {
            ids = twitter.getRetweeterIds(statusId, 500, cursor);
            idsSet.addAll(Arrays.asList(ArrayUtils.toObject(ids.getIDs())));
        } while ((cursor = ids.getNextCursor()) != 0);
        return idsSet;
    }
}
