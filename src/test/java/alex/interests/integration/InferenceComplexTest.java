package alex.interests.integration;

import alex.interests.data.extractor.MongoExtractor;
import alex.interests.data.model.Topic;
import alex.interests.data.model.Tweet;
import alex.interests.data.model.User;
import alex.interests.process.UserInterestsInferencer;
import alex.interests.process.params.AlgorithmParams;
import alex.interests.process.params.ReferenceType;
import alex.interests.process.params.RelationType;
import alex.interests.process.topic.TopicModel;
import alex.interests.process.topic.lda.LDAExtractor;
import alex.interests.process.topic.lda.LDAHelper;
import alex.interests.process.params.TopicInferenceParams;
import alex.interests.test.DataExtractorWrapper;
import alex.interests.test.PerplexityUtils;
import alex.interests.test.UserWrapper;
import cc.mallet.topics.TopicInferencer;
import cc.mallet.types.Instance;
import org.apache.commons.lang.ArrayUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.SortedMap;

public class InferenceComplexTest {
    private static final Logger log = LoggerFactory.getLogger(InferenceComplexTest.class);

    //need fix
    @Deprecated
    @Test
    public void inferenceTopicWithRegularizationAndPerplexityTesting() throws Exception {
        final Properties properties = new Properties();
        try (InputStream propsResource = getClass().getResourceAsStream("/test.resources.properties")) {
            properties.load(propsResource);
        }
        final String serializedModelFileName = properties.getProperty("model.file.name");
        final String serializedTopicDistributionFileName = properties.getProperty("distribution.file.name");
        final Path serializedModelFilePath = Paths.get(serializedModelFileName);
        final Path serializedTopicDistributionFilePath = Paths.get(serializedTopicDistributionFileName);
        final AlgorithmParams algorithmParams = new AlgorithmParams.AlgorithmParamsBuilder()
                .numIterations(2000)
                .numThreads(1)
                .numTopics(75)
                .build();
        final MongoExtractor mongoExtractor = MongoExtractor.getInstance();
        final User user = new DataExtractorWrapper(mongoExtractor, algorithmParams).getUserById("29970507");
        final TopicModel ownTopicForTrainTweetsModel;
        if (Files.exists(serializedModelFilePath) && Files.exists(serializedTopicDistributionFilePath)) {
            ownTopicForTrainTweetsModel = TopicModel.readModel(serializedModelFileName, serializedTopicDistributionFileName);
        } else {
            LDAExtractor ldaExtractor = new LDAExtractor(algorithmParams);
            ownTopicForTrainTweetsModel = ldaExtractor.extractUserOwnTopics(user);
            ownTopicForTrainTweetsModel.writeModel(serializedModelFileName, serializedTopicDistributionFileName);
        }
        UserInterestsInferencer interestsInferencer = new UserInterestsInferencer(mongoExtractor, algorithmParams);
        UserWrapper userWrapper = (UserWrapper) user;
        final List<Tweet> tweetsForTest = userWrapper.getTweetsForTest(true);

        //perform topic inference for test tweets
        final TopicInferencer topicInferencer = ownTopicForTrainTweetsModel.getTopicInferencer();
        final Instance instanceTest = LDAHelper.allTweetsToOneInstance(tweetsForTest);
        final TopicInferenceParams topicInferenceParams = algorithmParams.getTopicInferenceParams();
        final double[] sampledDistribution = topicInferencer.getSampledDistribution(instanceTest,
                topicInferenceParams.numIterations,
                topicInferenceParams.thinning,
                topicInferenceParams.burnIn);
        final List<Double> topicsDistrForTestTweets = Arrays.asList(ArrayUtils.toObject(sampledDistribution));
        final TopicModel ownTopicModel = new TopicModel(ownTopicForTrainTweetsModel.getTopics(),
                topicsDistrForTestTweets,
                ownTopicForTrainTweetsModel.getParallelTopicModel());

        //inout
        SortedMap<Double, Topic> calculatedTopicsInOut = interestsInferencer.inferenceTopics(user,
                RelationType.FOLLOWING, ReferenceType.ALL, ownTopicModel);
        final double perplexityWithInOutRegularization = PerplexityUtils.calculatePerplexity(tweetsForTest, calculatedTopicsInOut);
        log.info("perplexity inout regularization = {}", perplexityWithInOutRegularization);

        //none
        SortedMap<Double, Topic> calculatedTopicsNoReg = interestsInferencer.inferenceTopics(user,
                RelationType.FOLLOWING, ReferenceType.ALL, ownTopicModel);
        final double perplexityWithNoRegularization = PerplexityUtils.calculatePerplexity(tweetsForTest, calculatedTopicsNoReg);
        log.info("perplexity none regularization = {}", perplexityWithNoRegularization);

        //in
        SortedMap<Double, Topic> calculatedTopicsInReg = interestsInferencer.inferenceTopics(user,
                RelationType.FOLLOWING, ReferenceType.ALL, ownTopicModel);
        final double perplexityWithInRegularization = PerplexityUtils.calculatePerplexity(tweetsForTest, calculatedTopicsInReg);
        log.info("perplexity in regularization = {}", perplexityWithInRegularization);

        //out
        SortedMap<Double, Topic> calculatedTopicsOutReg = interestsInferencer.inferenceTopics(user,
                RelationType.FOLLOWING, ReferenceType.ALL, ownTopicModel);
        final double perplexityWithOutRegularization = PerplexityUtils.calculatePerplexity(tweetsForTest, calculatedTopicsOutReg);
        log.info("perplexity out regularization = {}", perplexityWithOutRegularization);

        //out retweets
        SortedMap<Double, Topic> calculatedTopicsRetweetsOut = interestsInferencer.inferenceTopics(user,
                RelationType.RETWEETS, ReferenceType.REFERENCES, ownTopicModel);
        final double perplexityWithRetweetOutRegularization = PerplexityUtils.calculatePerplexity(tweetsForTest, calculatedTopicsRetweetsOut);
        log.debug("perplexity out retweets regularization = {}", perplexityWithRetweetOutRegularization);

        log.info("Final results:");
        log.info("perplexity inout regularization = {}", perplexityWithInOutRegularization);
        log.info("perplexity none regularization = {}", perplexityWithNoRegularization);
        log.info("perplexity in regularization = {}", perplexityWithInRegularization);
        log.info("perplexity out regularization = {}", perplexityWithOutRegularization);
        log.info("perplexity retweet out regularization = {}", perplexityWithRetweetOutRegularization);
    }
}
