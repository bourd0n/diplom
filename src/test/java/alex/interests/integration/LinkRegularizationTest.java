package alex.interests.integration;

import alex.interests.data.extractor.MongoExtractor;
import alex.interests.data.model.Topic;
import alex.interests.data.model.Tweet;
import alex.interests.data.model.User;
import alex.interests.process.UserInterestsInferencer;
import alex.interests.process.params.AlgorithmParams;
import alex.interests.process.params.ReferenceType;
import alex.interests.process.params.RelationType;
import alex.interests.process.params.TopicInferenceParams;
import alex.interests.process.topic.TopicModel;
import alex.interests.process.topic.lda.LDAHelper;
import alex.interests.process.weight.RetweetLinkWeightCalculator;
import alex.interests.process.weight.ImplicitFollowingLinkWeightsCalculator;
import alex.interests.test.DataExtractorWrapper;
import alex.interests.test.PerplexityUtils;
import alex.interests.test.UserWrapper;
import cc.mallet.topics.TopicInferencer;
import cc.mallet.types.Instance;
import com.google.common.collect.*;
import org.apache.commons.lang.ArrayUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

public class LinkRegularizationTest {

    private static final Logger log = LoggerFactory.getLogger(LinkRegularizationTest.class);

    @Test
    public void testFindImplicitLinks() {
        final AlgorithmParams algorithmParams = new AlgorithmParams.AlgorithmParamsBuilder()
                .numIterations(2000)
                .numThreads(1)
                .numTopics(75)
                .build();
        final MongoExtractor mongoExtractor = MongoExtractor.getInstance();
        final User user = new DataExtractorWrapper(mongoExtractor, algorithmParams).getUserById("620601589");
        System.out.println("User " + user);
        final Set<String> friends = user.getFriends(true);
        ListMultimap<String, String> explicitLink2Authority = Multimaps.synchronizedListMultimap(ArrayListMultimap.create());
        friends.parallelStream().forEach(friendId -> {
            final Collection<String> friendFollowers = mongoExtractor.loadUserFollowers(friendId);
            friendFollowers.forEach(s -> explicitLink2Authority.put(s, friendId));
        });

        System.out.println(explicitLink2Authority.size());
    }

    @Test
    public void testFindImplicitInLinks() {
        final AlgorithmParams algorithmParams = new AlgorithmParams.AlgorithmParamsBuilder()
                .numIterations(2000)
                .numThreads(1)
                .numTopics(75)
                .build();
        final MongoExtractor mongoExtractor = MongoExtractor.getInstance();
        final User user = new DataExtractorWrapper(mongoExtractor, algorithmParams).getUserById("620601589");
        System.out.println("User " + user);

        final Map<String, Double> stringDoubleMap = new ImplicitFollowingLinkWeightsCalculator(mongoExtractor, algorithmParams).calculateInLinkWeights(user);
        System.out.println(stringDoubleMap.size());
        System.out.println(stringDoubleMap);
    }

    @Test
    public void testCalculateInLinkByRetweets() {
        final AlgorithmParams algorithmParams = new AlgorithmParams.AlgorithmParamsBuilder()
                .numIterations(2000)
                .numThreads(1)
                .numTopics(75)
                .build();
        final MongoExtractor mongoExtractor = MongoExtractor.getInstance();
        final User user = new DataExtractorWrapper(mongoExtractor, algorithmParams).getUserById("620601589");
        System.out.println("User " + user);

        final Map<String, Double> stringDoubleMap = new RetweetLinkWeightCalculator(mongoExtractor, algorithmParams, false).calculateInLinkWeights(user);
        System.out.println(stringDoubleMap.size());
        System.out.println(stringDoubleMap);
    }

    @Test
    public void inferenceTopicWithRegularizationAndPerplexityTesting_fixedUsers() throws Exception {
        final List<String> userIds = Lists.newArrayList("220145292"/*"34732119"*//*"134965175"*//*"84510198"*/);
        int numUsers = userIds.size();
        int numTopics = 75;

        final AlgorithmParams algorithmParamsInOutOwn = new AlgorithmParams.AlgorithmParamsBuilder()
                .numIterations(2000)
                .numThreads(1)
                .trainTweetsRatio(0.8)
                .numTopics(numTopics)
                .build();

        final MongoExtractor mongoExtractor = MongoExtractor.getInstance();
        final DataExtractorWrapper extractorWrapper = new DataExtractorWrapper(mongoExtractor, algorithmParamsInOutOwn);
        final Collection<User> users = userIds.stream().map(extractorWrapper::getUserById).collect(Collectors.toList());

        log.debug("Start test for users with ids {}", userIds);
        for (User user : users) {
            log.debug("Start test for {}", user);
            final TopicModel ownTopicForTrainTweetsModel = TestHelper.constructTopicModel(user.getId(), numTopics);
            UserWrapper userWrapper = (UserWrapper) user;
            final List<Tweet> tweetsForTest = userWrapper.getTweetsForTest(true);

            //perform topic inference for test tweets
            final TopicInferencer topicInferencer = ownTopicForTrainTweetsModel.getTopicInferencer();
            final Instance instanceTest = LDAHelper.allTweetsToOneInstance(tweetsForTest);
            final TopicInferenceParams topicInferenceParams = algorithmParamsInOutOwn.getTopicInferenceParams();
            final double[] sampledDistribution = topicInferencer.getSampledDistribution(instanceTest,
                    topicInferenceParams.numIterations,
                    topicInferenceParams.thinning,
                    topicInferenceParams.burnIn);
            final List<Double> topicsDistrForTestTweets = Arrays.asList(ArrayUtils.toObject(sampledDistribution));
            final TopicModel ownTopicModel = new TopicModel(ownTopicForTrainTweetsModel.getTopics(),
                    topicsDistrForTestTweets,
                    ownTopicForTrainTweetsModel.getParallelTopicModel());

            // inout + own
            SortedMap<Double, Topic> calculatedTopicsInOutOwn = new UserInterestsInferencer(mongoExtractor, algorithmParamsInOutOwn)
                    .inferenceTopics(user,
                            RelationType.FOLLOWING, ReferenceType.ALL, ownTopicModel);
            final double perplexityWithInOutOwnRegularization = PerplexityUtils.calculatePerplexity(tweetsForTest, calculatedTopicsInOutOwn);
            log.debug("perplexity inout own regularization = {}", perplexityWithInOutOwnRegularization);

            SortedMap<Double, Topic> calculatedTopicsInOutImplicitOwn = new UserInterestsInferencer(mongoExtractor, algorithmParamsInOutOwn)
                    .inferenceTopicsWithImplicitLinks(user,
                            RelationType.FOLLOWING, ownTopicModel);
            final double perplexityWithInOutOwnImplicitRegularization = PerplexityUtils.calculatePerplexity(tweetsForTest, calculatedTopicsInOutImplicitOwn);
            log.debug("perplexity inout implicit own regularization = {}", perplexityWithInOutOwnImplicitRegularization);

            log.info("Final results for {}", user);
            log.info("perplexity inout regularization = {}", perplexityWithInOutOwnRegularization);
            log.info("perplexity inout implicit regularization = {}", perplexityWithInOutOwnImplicitRegularization);
        }

    }

}
