package alex.interests.integration;

import alex.interests.data.extractor.MongoExtractor;
import alex.interests.data.model.Topic;
import alex.interests.data.model.Tweet;
import alex.interests.data.model.User;
import alex.interests.process.UserInterestsInferencer;
import alex.interests.process.params.AlgorithmParams;
import alex.interests.process.params.ReferenceType;
import alex.interests.process.params.RelationType;
import alex.interests.process.params.TopicInferenceParams;
import alex.interests.process.topic.TopicModel;
import alex.interests.process.topic.lda.LDAExtractor;
import alex.interests.process.topic.lda.LDAHelper;
import alex.interests.test.DataExtractorWrapper;
import alex.interests.test.PerplexityUtils;
import alex.interests.test.UserWrapper;
import cc.mallet.topics.TopicInferencer;
import cc.mallet.types.Instance;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.type.CollectionLikeType;
import com.google.common.collect.Iterables;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import static alex.interests.integration.ScheduleTestForUsers.RegType.*;

public class ScheduleTestForUsers {
    private static final Logger log = LoggerFactory.getLogger(ScheduleTestForUsers.class);
    private static final Logger resultLog = LoggerFactory.getLogger("resultLogger");

    private static final int numTopics = 75;
    private static final AlgorithmParams algorithmParamsInOut = new AlgorithmParams.AlgorithmParamsBuilder()
            .numIterations(2000)
            .numThreads(1)
            .numTopics(numTopics)
            .build();
    private static final AlgorithmParams algorithmParamsIn = new AlgorithmParams.AlgorithmParamsBuilder(algorithmParamsInOut)
            .coefInLinks(0.1)
            .coefOutLinks(0)
            .coefOwn(0.9)
            .build();
    private static final AlgorithmParams algorithmParamsOut = new AlgorithmParams.AlgorithmParamsBuilder(algorithmParamsInOut)
            .coefInLinks(0)
            .coefOutLinks(0.1)
            .coefOwn(0.9)
            .build();
    private static final AlgorithmParams algorithmParamsOwn = new AlgorithmParams.AlgorithmParamsBuilder(algorithmParamsInOut)
            .coefInLinks(0)
            .coefOutLinks(0)
            .coefOwn(1)
            .build();
    private static final MongoExtractor mongoExtractor = MongoExtractor.getInstance();

    public enum RegType {
        inout_explicit(algorithmParamsInOut),
        in_explicit(algorithmParamsIn),
        out_explicit(algorithmParamsOut),
        inout_implicit(algorithmParamsInOut),
        in_implicit(algorithmParamsIn),
        out_implicit(algorithmParamsOut),
        own(algorithmParamsOwn),
        rt_out_explicit(algorithmParamsOut);

        AlgorithmParams algorithmParams;

        RegType(AlgorithmParams algorithmParams) {
            this.algorithmParams = algorithmParams;
        }

        @Override
        public String toString() {
            return this.name();
        }
    }


    private Map<RegType, Double> perplexities;
    private String userId;
    private User user;
    private TopicModel ownTopicModel;
    private List<Tweet> tweetsForTest;

    @Before
    public void setUp() throws IOException {
        perplexities = new HashMap<>();
        userId = getNextId();
        if (userId == null) {
            return;
        }
        user = new DataExtractorWrapper(mongoExtractor, algorithmParamsInOut).getUserById(userId);
        log.debug("Start test for {}", user);
        final TopicModel ownTopicForTrainTweetsModel = new LDAExtractor(algorithmParamsInOut).extractUserOwnTopics(user);
        tweetsForTest = ((UserWrapper) user).getTweetsForTest(true);

        //perform topic inference for test tweets
        final TopicInferencer topicInferencer = ownTopicForTrainTweetsModel.getTopicInferencer();
        final Instance instanceTest = LDAHelper.allTweetsToOneInstance(tweetsForTest);
        final TopicInferenceParams topicInferenceParams = algorithmParamsInOut.getTopicInferenceParams();
        final double[] sampledDistribution = topicInferencer.getSampledDistribution(instanceTest,
                topicInferenceParams.numIterations,
                topicInferenceParams.thinning,
                topicInferenceParams.burnIn);
        final List<Double> topicsDistrForTestTweets = Arrays.asList(ArrayUtils.toObject(sampledDistribution));
        ownTopicModel = new TopicModel(ownTopicForTrainTweetsModel.getTopics(),
                topicsDistrForTestTweets,
                ownTopicForTrainTweetsModel.getParallelTopicModel());
    }

    @Test
    public void inferenceTopicWithRegularizationAndPerplexity() {
        if (userId == null) {
            return;
        }
        try {
            final long l1 = System.currentTimeMillis();
            calculatePerplexityFollowing(inout_explicit);
            final long l2 = System.currentTimeMillis();
            calculatePerplexityFollowing(own);
            final long l3 = System.currentTimeMillis();
            calculatePerplexityFollowing(in_explicit);
            final long l4 = System.currentTimeMillis();
            calculatePerplexityFollowing(out_explicit);
            final long l5 = System.currentTimeMillis();
            calculatePerplexityRetweeting(rt_out_explicit);
            final long l6 = System.currentTimeMillis();
            calculatePerplexityFollowingImplicit(inout_implicit);
            final long l7 = System.currentTimeMillis();
            calculatePerplexityFollowingImplicit(out_implicit);
            final long l8 = System.currentTimeMillis();
            calculatePerplexityFollowingImplicit(in_implicit);
            final long l9 = System.currentTimeMillis();

            resultLog.info("{} time {} ms", inout_explicit, l2 - l1);
            resultLog.info("{} time {} ms", own, l3 - l2);
            resultLog.info("{} time {} ms", in_explicit, l4 - l3);
            resultLog.info("{} time {} ms", out_explicit, l5 - l4);
            resultLog.info("{} time {} ms", rt_out_explicit, l6 - l5);
            resultLog.info("{} time {} ms", inout_implicit, l7 - l6);
            resultLog.info("{} time {} ms", out_implicit, l8 - l7);
            resultLog.info("{} time {} ms", in_implicit, l9 - l8);
            resultLog.info("Final results for {}", user);
            perplexities.entrySet().forEach(entry ->
                    resultLog.info("perplexity {} regularization = {}", entry.getKey(), entry.getValue()));

            TestHelper.writeToExcelWorkBook(user, numTopics, perplexities);
        } catch (Throwable throwable) {
            log.error("Exception for user " + userId, throwable);
            resultLog.error("Exception for user {}. Exception {}", userId, throwable.getMessage());
        }
    }

    protected double calculatePerplexityFollowing(RegType regType) {
        SortedMap<Double, Topic> calculatedTopics = new UserInterestsInferencer(mongoExtractor, regType.algorithmParams)
                .inferenceTopics(user,
                        RelationType.FOLLOWING, ReferenceType.ALL, ownTopicModel);
        double perplexity = PerplexityUtils.calculatePerplexity(tweetsForTest, calculatedTopics);
        log.debug("perplexity {} regularization = {}. Sum distr {}", regType, perplexity, getSum(calculatedTopics));
        perplexities.put(regType, perplexity);
        return perplexity;
    }

    private double getSum(SortedMap<Double, Topic> calculatedTopics) {
        return calculatedTopics.keySet().parallelStream().mapToDouble(value -> value).sum();
    }

    protected double calculatePerplexityFollowingImplicit(RegType regType) {
        SortedMap<Double, Topic> calculatedTopics = new UserInterestsInferencer(mongoExtractor, regType.algorithmParams)
                .inferenceTopicsWithImplicitLinks(user,
                        RelationType.FOLLOWING, ownTopicModel);
        double perplexity = PerplexityUtils.calculatePerplexity(tweetsForTest, calculatedTopics);
        log.debug("perplexity {} regularization = {}. Sum distr {}", regType, perplexity, getSum(calculatedTopics));
        perplexities.put(regType, perplexity);
        return perplexity;
    }

    protected double calculatePerplexityRetweeting(RegType regType) {
        SortedMap<Double, Topic> calculatedTopics = new UserInterestsInferencer(mongoExtractor, regType.algorithmParams)
                .inferenceTopics(user,
                        RelationType.RETWEETS, ReferenceType.REFERENCES, ownTopicModel);
        double perplexity = PerplexityUtils.calculatePerplexity(tweetsForTest, calculatedTopics);
        log.debug("perplexity {} regularization = {}. Sum distr {}", regType, perplexity, getSum(calculatedTopics));
        perplexities.put(regType, perplexity);
        return perplexity;
    }

    private String getNextId() throws IOException {
        final Properties properties = new Properties();
        try (InputStream propsResource = getClass().getResourceAsStream("/test.resources.properties")) {
            properties.load(propsResource);
        }
        final String userToProcessFile = properties.getProperty("users.to.process.file");

        ObjectMapper objectMapper = new ObjectMapper()
                .configure(SerializationFeature.INDENT_OUTPUT, true)
                .configure(SerializationFeature.WRITE_NULL_MAP_VALUES, true)
                .setSerializationInclusion(JsonInclude.Include.NON_NULL);
        final CollectionLikeType type = objectMapper.getTypeFactory().constructCollectionType(ArrayList.class, String.class);

        final File file = new File(userToProcessFile);
        List<String> usersToProcess = objectMapper.readValue(file, type);

        if (CollectionUtils.isEmpty(usersToProcess)) {
            return null;
        }
        final String lastId = Iterables.getLast(usersToProcess);
        usersToProcess.remove(lastId);
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(file, usersToProcess);

        return lastId;
    }

}
