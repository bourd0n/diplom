package alex.interests.integration;

import alex.interests.data.extractor.MongoExtractor;
import alex.interests.data.model.ReTweet;
import alex.interests.data.model.User;
import alex.interests.process.params.AlgorithmParams;
import alex.interests.process.topic.TopicModel;
import alex.interests.process.topic.lda.LDAExtractor;
import alex.interests.test.DataExtractorWrapper;
import alex.interests.test.UserWrapper;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.common.collect.ListMultimap;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import static alex.interests.integration.ScheduleTestForUsers.RegType;

public class TestHelper {

    private static final Logger log = LoggerFactory.getLogger(TestHelper.class);

    static {
        try (InputStream propsResource = TestHelper.class.getResourceAsStream("/test.resources.properties")) {
            Properties properties = new Properties();
            properties.load(propsResource);
            excelResultFile = properties.getProperty("excel.result.file");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static String excelResultFile;

    public static TopicModel constructTopicModel(String userId, int numTopics) throws IOException {
        final Properties properties = new Properties();
        try (InputStream propsResource = TestHelper.class.getResourceAsStream("/test.resources.properties")) {
            properties.load(propsResource);
        }
        final String serializedModelFileName = properties.getProperty("model.file.name") + userId;
        final String serializedTopicDistributionFileName = properties.getProperty("distribution.file.name") + userId;
        final Path serializedModelFilePath = Paths.get(serializedModelFileName);
        final Path serializedTopicDistributionFilePath = Paths.get(serializedTopicDistributionFileName);
        final AlgorithmParams algorithmParams = new AlgorithmParams.AlgorithmParamsBuilder()
                .numIterations(2000)
                .numThreads(1)
                .trainTweetsRatio(0.8)
                .numTopics(numTopics)
                .build();
        final MongoExtractor mongoExtractor = MongoExtractor.getInstance();
        final User user = new DataExtractorWrapper(mongoExtractor, algorithmParams).getUserById(userId);
        final TopicModel ownTopicForTrainTweetsModel;
        if (Files.exists(serializedModelFilePath) && Files.exists(serializedTopicDistributionFilePath)) {
            ownTopicForTrainTweetsModel = TopicModel.readModel(serializedModelFileName, serializedTopicDistributionFileName);
        } else {
            LDAExtractor ldaExtractor = new LDAExtractor(algorithmParams);
            ownTopicForTrainTweetsModel = ldaExtractor.extractUserOwnTopics(user);
            ownTopicForTrainTweetsModel.writeModel(serializedModelFileName, serializedTopicDistributionFileName);
        }
        return ownTopicForTrainTweetsModel;
    }

    public static void writeToExcelWorkBook(User user, int numTopics, Map<ScheduleTestForUsers.RegType, Double> perplexities) {

        try {
            FileInputStream file = new FileInputStream(new File(excelResultFile));

            HSSFWorkbook workbook = new HSSFWorkbook(file);
            HSSFSheet sheet = workbook.getSheet(String.valueOf(numTopics));

            final int lastRowNum = sheet.getLastRowNum();
            final HSSFRow row = sheet.createRow(lastRowNum + 1);

            row.createCell(0).setCellValue(user.getId());
            row.createCell(1).setCellValue(user.getFollowers(true).size());
            row.createCell(2).setCellValue(user.getFriends(true).size());
            final ListMultimap<String, ReTweet> retweetedUsers = user.getRetweetedUsers(true);
            row.createCell(3).setCellValue(retweetedUsers.values().size());
            row.createCell(4).setCellValue(retweetedUsers.keySet().size());
            int tweetsSize = (user instanceof UserWrapper ? ((UserWrapper) user).getWrappedUser() : user).getTweets(true).size();
            row.createCell(5).setCellValue(tweetsSize);
            row.createCell(6).setCellValue(perplexities.get(RegType.inout_explicit));
            row.createCell(7).setCellValue(perplexities.get(RegType.in_explicit));
            row.createCell(8).setCellValue(perplexities.get(RegType.out_explicit));
            row.createCell(9).setCellValue(perplexities.get(RegType.inout_implicit));
            row.createCell(10).setCellValue(perplexities.get(RegType.in_implicit));
            row.createCell(11).setCellValue(perplexities.get(RegType.out_implicit));
            row.createCell(12).setCellValue(perplexities.get(RegType.rt_out_explicit));
            row.createCell(13).setCellValue(perplexities.get(RegType.own));

            file.close();

            FileOutputStream outFile = new FileOutputStream(new File(excelResultFile));
            workbook.write(outFile);
            outFile.close();

        } catch (IOException e) {
            log.error("Exception occurred during saving to excel file", e);
        }
    }


    @Test
    public void writeToUsersToProcessFile() throws IOException {
        List<String> alreadyProcessedUsers = new ArrayList<>();
        final Properties properties = new Properties();
        try (InputStream propsResource = getClass().getResourceAsStream("/test.resources.properties")) {
            properties.load(propsResource);
        }
        try (final InputStream fis = new FileInputStream(properties.getProperty("processed_users.file.name"))) {
            BufferedReader in = new BufferedReader(new InputStreamReader(fis));
            String line;
            while ((line = in.readLine()) != null) {
                alreadyProcessedUsers.add(line);
            }
        }
        alreadyProcessedUsers = alreadyProcessedUsers.stream().limit(50).collect(Collectors.toList());
        final String userToProcessFile = properties.getProperty("users.to.process.file");
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        objectMapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, true);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        final File file = new File(userToProcessFile);
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(file, alreadyProcessedUsers);
    }
}
