package alex.interests.integration;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DummyTest {
    private static final Logger log = LoggerFactory.getLogger(DummyTest.class);

    @Test
    public void test() throws InterruptedException {
        log.debug("DummyTest run");
        Thread.sleep(360000);
        log.debug("DummyTest finish");
    }

    @Test
    public void test1() {
        Pattern pattern = Pattern.compile("(?:((https?|ftp|file|htttps?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]))|(\\p{L}+[#$\\*\\-@]*[\\p{L}\\d]+)|(\\p{L})\n");
        Matcher matcher = pattern.matcher("rt @sdfsdf: sdf http://adsasd.com");
        while (matcher.find()){
            System.out.println(matcher.group());
        }
    }

    @Test
    public void testFormat() throws ParseException {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", Locale.ENGLISH);
        final Date parse = simpleDateFormat.parse("Mon Aug 05 19:38:36 +0000 2013");
        System.out.println(parse);
    }
}
